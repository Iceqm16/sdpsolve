#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name = "sdpsolve",
    version = "0.0.0",
    author = "Nicholas C Rubin",
    author_email = "rubinnc0@gmail.com",
    description = "Building and solving semidefinite programs",
    packages = find_packages(),
    install_requires = [
                        'numpy >= 1.10',
                        'scipy'],
)
