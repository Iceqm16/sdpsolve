"""
Read and solve the SDP
"""
import os
from sdpsolve.sdp import sdp
import sdpsolve.solvers.bpsdp as bpsdp

if __name__ == "__main__":
    s = sdp.SDP()
    s.sdpfile = os.path.join(os.getcwd(), "HubK1DN6U5DQG.sdp")
    s.Initialize()
    s.inner_solve = "EXACT"
    s.epsilon = 1.0E-7
    bpsdp.solve_bpsdp(s)