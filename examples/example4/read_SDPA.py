"""
Read output from SDPA
"""
from sdpsolve.sdp import sdp
from sdpsolve.sdp.io_sdpsolve import readSDPAout

prog = sdp.SDP()
prog.sdpfile = "./Hub1DN4U0DQG.sdp"
prog.Initialize()

result = readSDPAout("./Hub1DN4U0DQG.sdpa.sol", "./Hub1DN4U0DQG.sdpa.out", prog)
print result['XZgap']
print result['success']
print result['pObj']
print result['dObj']
print result.keys()
print len(result['PrimalMats'])

