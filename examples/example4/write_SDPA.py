from sdpsolve.sdp import sdp
from sdpsolve.sdp.io_sdpsolve import writeSDPA

prog = sdp.SDP()
prog.sdpfile = "./Hub1DN4U0DQG.sdp"
prog.Initialize()

writeSDPA(prog, "Hub1DN4U0DQG.sdpa")
