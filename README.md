# SDPSolve
Package for solving semidefinite programs with various algorithms
-----------------------------------------------------------------

SDPSolve is a parent package to various SDP solvers. Currently RRSDP([ref](http://link.springer.com/article/10.1007%2Fs10107-002-0352-8)) and BPSDP([ref](http://link.springer.com/article/10.1007/s00607-006-0182-2)) are implemented for solving the SDP.


`src` contains python code and `examples` contains examples involving SDPs
for Hubbard models and sum-of-squares polynomial certification.

## Dependencies
------
* numpy 1.9.3
* scipy 0.15.0

## To Do
------
* Add optional parallelism to positive-semidefinite projection step in BPSDP method
