Norm = "L1"
eta = 0.8
DIIS = False
DIIS_length = 4
NESTEROV = False
epsilon = float(0.5E-5)
iter_max = 10000
inner_solve = "CG"
