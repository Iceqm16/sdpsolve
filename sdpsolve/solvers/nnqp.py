"""
Solve a non-negative quadratic program with cvxpy
"""
import numpy as np
import cvxpy as cvx
from sdpsolve.solvers.alternating_projections import opdm_projection_boreland_dennis
from scipy.optimize import minimize


def obj(alpha, array1, array2, base_array):
    alpha0 = alpha[0]
    return np.linalg.norm(array1 * alpha0 + (1 - alpha0) * array2 - base_array)


if __name__ == "__main__":

    vrdm_opdm = np.array([[0.66666667,  0.        , 0.08128482, 0.        , 0.08128482, 0.        ],
                          [0.        ,  0.33333333, 0.        , 0.10729911, 0.        , 0.10729911],
                          [0.08128482,  0.        , 0.66666667, 0.        , 0.08128482, 0.        ],
                          [0.        ,  0.10729911, 0.        , 0.33333333, 0.        , 0.10729911],
                          [0.08128482,  0.        , 0.08128482, 0.        , 0.66666667, 0.        ],
                          [0.        ,  0.10729911, 0.        , 0.10729911, 0.        , 0.33333333]])
    true_opdm = np.array([[ 0.92064791, 0.        , 0.09921513, 0.        , -0.02130591, 0.        ],
                          [ 0.        , 0.08715753, 0.        , 0.119398  ,  0.        , 0.03807369],
                          [ 0.09921513, 0.        , 0.35889448, 0.        ,  0.16594523, 0.        ],
                          [ 0.        , 0.119398  , 0.        , 0.63164696,  0.        , 0.16442565],
                          [-0.02130591, 0.        , 0.16594523, 0.        ,  0.72045761, 0.        ],
                          [ 0.        , 0.03807369, 0.        , 0.16442565,  0.        , 0.28119551]])

    # assert np.allclose(test_true_opdms[0], true_opdm[::2, ::2], atol=1.0E-8)
    # assert np.allclose(test_true_opdms[1], true_opdm[1::2, 1::2], atol=1.0E-8)

    true_opdm2 = np.array([[ 0.41268543,  0.        ,  0.0633545 ,  0.        ,  0.18387555,  0.        ],
                          [ 0.        ,  0.57950914,  0.        ,  0.09520022,  0.        ,  0.17652454],
                          [ 0.0633545 ,  0.        ,  0.97443885,  0.        , -0.0033756 ,  0.        ],
                          [ 0.        ,  0.09520022,  0.        ,  0.0350197 ,  0.        ,  0.05017258],
                          [ 0.18387555,  0.        , -0.0033756 ,  0.        ,  0.61287572,  0.        ],
                          [ 0.        ,  0.17652454,  0.        ,  0.05017258,  0.        ,  0.38547116]])

    results = minimize(obj, [0], args=(true_opdm, true_opdm2, vrdm_opdm))
    print(results)
    print(true_opdm * 0.5 + true_opdm2 * 0.5)
    exit()
    true_opdm_a = true_opdm[::2, ::2]
    true_opdm_b = true_opdm[1::2, 1::2]


    # first get eigenvalues of each submatrix
    w_opdm_a, v_opdm_a = np.linalg.eigh(vrdm_opdm[::2, ::2])
    w_opdm_b, v_opdm_b = np.linalg.eigh(vrdm_opdm[1::2, 1::2])
    # now sort eigvals and eig vecs in descending order
    order = np.argsort(np.hstack((w_opdm_a, w_opdm_b)))[::-1]  # descending order
    w_opdm = np.hstack((w_opdm_a, w_opdm_b))[order]
    v_opdm = np.hstack((np.vstack((v_opdm_a, np.zeros_like(v_opdm_a))), np.vstack((np.zeros_like(v_opdm_b), v_opdm_b))))
    v_opdm = v_opdm[:, order]

    # Boreland-Dennis constraints
    # λ3 − λ4 − λ5 ≤ 0
    # λ0 + λ5 = 1
    # λ1 + λ4 = 1
    # λ2 + λ3 = 1
    print(w_opdm[3] - w_opdm[4] - w_opdm[5])
    print(w_opdm[0] + w_opdm[5])
    print(w_opdm[1] + w_opdm[4])
    print(w_opdm[2] + w_opdm[3])

    # eigs_target = np.array(list(reversed([3.0/5, 1.0/2, 7.0/20, 1.0/10, -11.0/20])))
    dim = true_opdm.shape[0]
    trace_target = 3  # Boreland-Dennis constraint for ^3 H
    cx = cvx.Variable(dim)
    constraints = [cx >= 0, cx <= 1, cvx.sum(cx) == trace_target,
                   cx[3] - cx[4] - cx[5] <= 0,
                   cx[0] + cx[5] == 1,
                   cx[1] + cx[4] == 1,
                   cx[2] + cx[3] == 1]
    objective = cvx.Minimize(cvx.sum_squares(cx - w_opdm))
    problem = cvx.Problem(objective, constraints)
    problem.solve(solver='SCS', verbose=True, eps=1.0E-8)

    print(cx.value)
    print(cx.value[3] - cx.value[4] - cx.value[5])
    print(cx.value[0] + cx.value[5])
    print(cx.value[1] + cx.value[4])
    print(cx.value[2] + cx.value[3])

    # generate new 1-RDM in the original ordering
    new_opdm = v_opdm[:, order] @ np.diag(cx.value[order]) @ v_opdm[:, order].T
    new_opdm_a = new_opdm[:3, :3]
    new_opdm_b = new_opdm[3:, 3:]

