'''
BPSDP algorithm

Author: Nicholas C Rubin
Paper: Computing 78(3): 277-286 November 2006
'''

# MAJOR LIBRARY IMPORTS
import scipy as sp
import time

# OPTIMIZATION ROUTINE AND SPARSE IMPORTS
# from scipy.sparse import csr_matrix, coo_matrix, csc_matrix
from scipy.sparse.linalg import cg as cg_solve
from scipy.sparse.linalg import bicgstab

# PROJECT IMPORTS
from sdpsolve.utils.RDMUtils import *
from sdpsolve.utils.matreshape import *
from sdpsolve.sdp import sdp as sdp_mod


class ConvergenceWarning(Warning):
    pass


class SdpData(object):
    def __init__(self, sdp_obj):
        self.sdp_obj = sdp_obj
        self.primal_blocks = []
        for i in sdp_obj.blockstruct:
            self.primal_blocks.append(np.eye(i).flatten(order='F'))
        self.primal_vector = np.require(np.hstack(self.primal_blocks), dtype=float, requirements=['F', 'A', 'W', 'O']).reshape((-1, 1))
        self.dual_y = np.require(np.asfortranarray(np.zeros((sdp_obj.nc, 1))), dtype=float,
                       requirements=['F', 'A', 'W', 'O'])

        self.sigma = 5
        self.mu = 1. / self.sigma
        self.gamma = 10  # np.sqrt(10)

        self.z_vector = -1 * np.copy(sdp_obj.cvec).reshape((-1, 1))
        self.z_blocks = vec2block(sdp_obj.blockstruct, self.z_vector)
        self.c_blocks = vec2block(sdp_obj.blockstruct, sdp_obj.cvec)
        self.AAt = sdp_obj.Amat @ sdp_obj.Amat.T

    def residual(self):
        return self.sdp_obj.bvec - self.sdp_obj.Amat @ self.primal_vector

    def dual_residual(self):
        return self.sdp_obj.Amat.T @ self.dual_y - self.sdp_obj.cvec + self.z_vector

    def primal_value(self):
        return self.sdp_obj.cvec.T @ self.primal_vector

    def dual_value(self):
        return self.sdp_obj.bvec.T @ self.dual_y

    def gap(self):
        pv = self.primal_value()
        dv = self.dual_value()
        return abs(pv - dv) / (1 + abs(pv) + abs(dv))


def yzupdate(sdp_obj, sdp_data, tau=1.6):
    inner_iter = 0
    residual = sdp_data.residual().reshape((-1, 1))
    update_epsilon_old = np.Inf
    # print(update_epsilon_old)
    current_primal = np.copy(sdp_data.primal_vector)
    while inner_iter < sdp_obj.inner_iter_max:
        right_hand_side = sdp_obj.Amat @ (sdp_obj.cvec - sdp_data.z_vector) + tau * sdp_data.mu * residual
        y, solve_status = cg_solve(sdp_data.AAt, right_hand_side, x0=sdp_data.dual_y, tol=sdp_obj.epsilon_inner/10)
        if solve_status != 0:
            raise ValueError("solver didn't solve!")
        sdp_data.dual_y = np.copy(np.reshape(y, (-1, 1)))
        u = sdp_data.mu * current_primal + sdp_obj.Amat.T @ sdp_data.dual_y - sdp_obj.cvec
        u_blocks = vec2block(sdp_obj.blockstruct, u)
        for ii in range(len(u_blocks)):
            w, v = np.linalg.eigh(u_blocks[ii])
            idxp = np.where(w >= 0)[0]
            idxm = np.where(w < 0)[0]
            positive_projection = v[:, idxp] @ np.diag(w[idxp]) @ v[:, idxp].T
            negative_projection = v[:, idxm] @ np.diag(w[idxm]) @ v[:, idxm].T

            # update V vector
            sdp_data.primal_blocks[ii] = positive_projection / sdp_data.mu
            sdp_data.z_blocks[ii] = -1 * negative_projection

        sdp_data.primal_vector = block2vec(sdp_data.primal_blocks)
        sdp_data.z_vector = block2vec(sdp_data.z_blocks)

        update_residual = sdp_obj.bvec - sdp_obj.Amat @ sdp_data.primal_vector
        update_epsilon = np.sqrt(update_residual.T @ update_residual)

        # print(update_epsilon, update_epsilon < sdp_obj.epsilon_inner)
        if update_epsilon < sdp_obj.epsilon_inner or update_epsilon == update_epsilon_old:
            break

        update_epsilon_old = update_epsilon

        inner_iter += 1

    return sdp_data.primal_vector, sdp_data.dual_y, sdp_data.z_vector


def solve_bpsdp(sdp_obj, sdp_data=None):
    """
    Solve a SDP with the boundary point method
    """
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError(
            "SDP input {} is not an sdpsolve SDP object".format(sdp_obj.__repr__))

    if sdp_data is None:
        sdp_data = SdpData(sdp_obj)

    master_start = time.time()
    norm_bvec = np.sqrt(sdp_obj.bvec.T @ sdp_obj.bvec)[0, 0]
    norm_cvec = np.sqrt(sdp_obj.cvec.T @ sdp_obj.cvec)[0, 0]

    residual = sdp_data.residual()
    epsilon = np.sqrt(residual.T @ residual)
    dual_residual= sdp_obj.Amat.T @ sdp_data.dual_y - sdp_obj.cvec + sdp_data.z_vector
    dual_epsilon = np.sqrt(dual_residual.T @ dual_residual)

    update_frequency = 300 # update sigma every update_it iterations
    iterc = 0
    stopping_epsilon = np.Inf

    while stopping_epsilon > sdp_obj.epsilon and iterc < sdp_obj.iter_max:
        p, y, z = yzupdate(sdp_obj, sdp_data)
        sdp_data.primal_vector = p
        sdp_data.dual_y = y
        sdp_data.z_vector = z

        residual = sdp_data.residual()
        dual_residual = sdp_data.dual_residual()
        gap = sdp_data.gap()

        epsilon = np.sqrt(residual.T @ residual)[0, 0]
        dual_epsilon = np.sqrt(dual_residual.T @ dual_residual)[0, 0]

        rel_err_p = epsilon / (1 + norm_bvec)
        rel_err_d = dual_epsilon / (1 + norm_cvec)
        stopping_epsilon = max(rel_err_p, rel_err_d, gap)

        if iterc % update_frequency == 0 or abs(np.log(rel_err_p/rel_err_d)) >= 3.0:
            sdp_data.mu = sdp_data.mu * rel_err_p / rel_err_d

        if sdp_obj.disp:
            print("%i\t%2.6e\t%2.6e\t%2.6e\t%2.6e\t%2.6e" % (iterc, sdp_data.primal_value(),
                                                             sdp_data.dual_value(),
                                                             rel_err_p, rel_err_d,
                                                             1 / sdp_data.mu))
        iterc += 1

    sdp_obj.primal = sdp_data.primal_vector
    return sdp_data

if __name__ == "__main__":
    import os
    from sdpsolve.sdp.io_sdpsolve import readSDP
    from scipy.sparse import coo_matrix
    from sdpsolve.solvers.bpsdp import solve_bpsdp as sbpsdp
    LIBRARY_ROOT = "/Users/lobster/opt/sdpsolve/sdpsolve/"
    # LIBRARY_ROOT = "/Users/lobster/opt/sdpsolve"
    # filename = os.path.join(LIBRARY_ROOT, "sdp/theta1.dat-s.sdp")
    # filename = os.path.join(LIBRARY_ROOT, "sdp/truss1.dat-s.sdp")
    # filename = os.path.join(LIBRARY_ROOT, "examples/example7/HubK1DN6U5DQG.sdp")
    filename = os.path.join(LIBRARY_ROOT, "sdp/Hub1DN3U10_s0.5_0.5.sdp")
    blockstruct, Amatrow, Amatcol, Amatarc, bvec, cvec, nc, nv, nnz, nb = readSDP(filename)

    Amat = coo_matrix((Amatarc, (Amatrow, Amatcol)), shape=(nc, nv)).tocsr()

    sdp_obj = sdp_mod.SDP()
    sdp_obj.nc = nc
    sdp_obj.nv = nv
    sdp_obj.nnz = nnz
    sdp_obj.nb = nb
    sdp_obj.blockstruct = blockstruct
    sdp_obj.cvec = cvec.reshape((-1, 1))
    sdp_obj.bvec = bvec.reshape((-1, 1))
    sdp_obj.Amat = Amat

    sdp_obj.Initialize()
    sdp_obj.inner_solve = 'CG'
    sdp_obj.epsilon = 1.0E-6
    sdp_obj.epsilon_inner = sdp_obj.epsilon_inner / 100
    sdp_obj.iter_max = 10000
    sdp_obj.inner_iter_max = 100
    sdp_obj.admm_style = False


    # sbpsdp(sdp_obj)
    sdp_data = solve_bpsdp(sdp_obj)
