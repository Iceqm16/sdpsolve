"""
Author: Nicholas C Rubin
"""

# OPTIMIZATION ROUTINE AND SPARSE IMPORTS
import scipy
from scipy.sparse.linalg import cg as cg_solve
from scipy.sparse.linalg import inv as sp_sparse_inv
from scipy.sparse import csc_matrix, coo_matrix

# PROJECT IMPORTS
from sdpsolve.utils.RDMUtils import *
from sdpsolve.utils.matreshape import *
from sdpsolve.sdp import sdp as sdp_mod
import numpy as np

from collections import deque
import cvxpy as cvx

from sdpsolve.solvers.bpsdp2 import solve_bpsdp

DYKSTRA_ITERMAX = 1000 # np.Inf

class ConvergenceWarning(Warning):
    pass


class SdpData(object):
    def __init__(self, sdp_obj):
        self.sdp_obj = sdp_obj
        self.primal_blocks = []
        for i in sdp_obj.blockstruct:
            self.primal_blocks.append(np.eye(i).flatten(order='F'))
        self.primal_vector = np.require(np.hstack(self.primal_blocks), dtype=float, requirements=['F', 'A', 'W', 'O']).reshape((-1, 1))
        self.dual_y = np.require(np.asfortranarray(np.zeros((nc, 1))), dtype=float,
                       requirements=['F', 'A', 'W', 'O'])

        self.sigma = 5
        self.mu = 1. / self.sigma
        self.gamma = 10  # np.sqrt(10)

        self.z_vector = -1 * np.copy(sdp_obj.cvec).reshape((-1, 1))
        self.z_blocks = vec2block(sdp_obj.blockstruct, self.z_vector)
        self.c_blocks = vec2block(sdp_obj.blockstruct, sdp_obj.cvec)
        self.AAt = sdp_obj.Amat @ sdp_obj.Amat.T

    def residual(self):
        return self.sdp_obj.bvec - self.sdp_obj.Amat @ self.primal_vector

    def dual_residual(self):
        return self.sdp_obj.Amat.T @ self.dual_y - self.sdp_obj.cvec + self.z_vector

    def primal_value(self):
        return self.sdp_obj.cvec.T @ self.primal_vector

    def dual_value(self):
        return self.sdp_obj.bvec.T @ self.dual_y

    def gap(self):
        pv = self.primal_value()
        dv = self.dual_value()
        return abs(pv - dv) / (1 + abs(pv) + abs(dv))


def solve_linear_projection(sdp_obj, sdp_data):
    """
    Solve the projection of the current vector onto the set of linnear constraints

    min 0.5 ||x - z||^{2} s.t. Ax = b
    z is the matrix to project

    get equations Ax = b and x - z + A^{T}y = 0
    Ax - Az + AA^{T}y = 0
    b - Az + AA^{T} y = 0
    AA^{T}y = Az - b
    solve AA^{T} y = Az - b via CG
    x = z - A^{T}(y)



    :param sdp_obj:
    :param sdp_data:
    :return:
    """
    # we might have faster convergence if we seed the CG but we also get numerical
    # instabilities for some reason...!?
    y, status = cg_solve(sdp_obj.Amat @ sdp_obj.Amat.T,
                         sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec,
                         # x0=sdp_data.dual_y,
                         tol=sdp_obj.epsilon_inner)
    if status != 0:
        raise ValueError("CG Solve didn't work")  # I know...throw a real error

    sdp_data.dual_y = np.copy(y.reshape((-1, 1)))
    # sdp_data.dual_y = sp_sparse_inv(sdp_data.AAt) @ (sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec)

    # sdp_data.primal_vector = sdp_data.primal_vector - sdp_obj.Amat.T @ sdp_data.dual_y
    sdp_data.primal_vector -= sdp_obj.Amat.T @ sdp_data.dual_y
    # print("CG Status ", status)
    # if np.linalg.norm(sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec) > sdp_obj.epsilon_inner:
    #     print(np.linalg.norm(sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec))
    #     print(np.linalg.norm(sdp_obj.Amat @ sdp_obj.Amat.T @ sdp_data.dual_y - (sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec)))
    #     print(sdp_obj.epsilon_inner)
    #     raise ValueError("something is wrong with our solution to the linear system")

    return sdp_data


def solve_positive_projection(sdp_obj, sdp_data):
    """
    Solve the positive projection problem

    min 0.5||x - z||^{2} s.t. x >= 0

    x is positive semidefinite

    note: need to turn the vector into blocks first then back to vector
    :param sdp_data:
    :return:
    """
    sdp_data.primal_blocks = vec2block(sdp_obj.blockstruct, sdp_data.primal_vector)
    for idx_rdm, rdm in enumerate(sdp_data.primal_blocks):
        w, v = np.linalg.eigh(rdm)
        idxp = np.where(w >= -float(1.0E-14))[0]
        positive_projection = v[:, idxp] @ np.diag(w[idxp]) @ v[:, idxp].T
        sdp_data.primal_blocks[idx_rdm] = positive_projection

    sdp_data.primal_vector = block2vec(sdp_data.primal_blocks)
    return sdp_data


def opdm_projection_boreland_dennis(opdms, epsilon=1.0E-8, verbose=False):
    """
    project the opdms to pure state

    :param opdms: list of opdms.  if length 1 then just spin-orbital 1-RDM.
                  if length 2 then alpha-block is first then beta-block
    :return: projected RDMs in a similar list as the input
    """
    if len(opdms) == 2:
       w_opdm_0, v_opdm_0 = np.linalg.eigh(opdms[0])
       w_opdm_1, v_opdm_1 = np.linalg.eigh(opdms[1])

       # now sort eigvals and eig vecs in descending order
       order = np.argsort(np.hstack((w_opdm_0, w_opdm_1)))[::-1]  # descending order
       w_opdm = np.hstack((w_opdm_0, w_opdm_1))[order]
       v_opdm = np.hstack((np.vstack((v_opdm_0, np.zeros_like(v_opdm_0))), np.vstack((np.zeros_like(v_opdm_1), v_opdm_1))))
       v_opdm = v_opdm[:, order]
    elif len(opdms) == 1:
        w_opdm, v_opdm = np.linalg.eigh(opdms[0])
    else:
        raise TypeError("list for 1-RDM index should be length 1 or 2")

    # Boreland-Dennis constraints
    # λ3 − λ4 − λ5 ≤ 0
    # λ0 + λ5 = 1
    # λ1 + λ4 = 1
    # λ2 + λ3 = 1
    # print(w_opdm[3] - w_opdm[4] - w_opdm[5])
    # print(w_opdm[0] + w_opdm[5])
    # print(w_opdm[1] + w_opdm[4])
    # print(w_opdm[2] + w_opdm[3])

    # solve the system to get the eigenvalue projection to closest pure state 1-RDM
    dim = 6
    trace_target = 3  # Boreland-Dennis constraint for ^3 H
    cx = cvx.Variable(dim)
    constraints = [cx >= 0, cx <= 1, cvx.sum(cx) == trace_target,
                   cx[3] - cx[4] - cx[5] <= 0,
                   cx[0] + cx[5] == 1,
                   cx[1] + cx[4] == 1,
                   cx[2] + cx[3] == 1]
    objective = cvx.Minimize(cvx.sum_squares(cx - w_opdm))
    problem = cvx.Problem(objective, constraints)
    problem.solve(solver='SCS', verbose=verbose, eps=epsilon)

    if len(opdms) == 2:
        # generate new 1-RDM in the original ordering
        new_opdm = v_opdm[:, order] @ np.diag(cx.value[order]) @ v_opdm[:, order].T
        new_opdm_0 = new_opdm[:3, :3]
        new_opdm_1 = new_opdm[3:, 3:]
        opdms[0] = new_opdm_0
        opdms[1] = new_opdm_1
    else:
        new_opdm = v_opdm[:, ::-1] @ np.diag(cx.value[::-1]) @ v_opdm[:, ::-1].T
        opdms[0] = new_opdm

    return opdms


def solve_positive_projection_boreland_dennis(sdp_obj, sdp_data, opdm_idx=[0, 1]):
    """
    Solve the positive projection problem

    min 0.5 ||x - z||^{2} s.t. x >= 0 lambda(x) in pure state

    :param sdp_obj:
    :param sdp_data:
    :param opdm_idx: indices of the 1-RDMs in the blockstruct
    :return:
    """
    rdms = vec2block(sdp_obj.blockstruct, sdp_data.primal_vector)

    if len(opdm_idx) == 2:
        opdms = opdm_projection_boreland_dennis([rdms[opdm_idx[0]], rdms[opdm_idx[1]]])
        rdms[opdm_idx[0]] = opdms[0]
        rdms[opdm_idx[1]] = opdms[1]
    elif len(opdm_idx) == 1:
        opdms = opdm_projection_boreland_dennis([rdms[opdm_idx[0]]])
        rdms[opdm_idx[0]] = opdms[0]
    else:
        raise TypeError("opdm_idx should be length 1 or 2")

    for idx, rdm in enumerate(rdms):
        if idx not in opdm_idx:
            w, v = np.linalg.eigh(rdm)
            idxp = np.where(w >= -float(1.0E-14))[0]
            positive_projection = v[:, idxp] @ np.diag(w[idxp]) @ v[:, idxp].T
            rdms[idx] = positive_projection

    sdp_data.primal_blocks = rdms
    sdp_data.primal_vector = block2vec(rdms)
    return sdp_data


def solve_alternating_projections_bregman(sdp_obj, sdp_data, epsilon=1.0E-8, pure_state=False, disp=False):
    """
    Solve projection onto positive semidefinite set intersected with an affine subspace
    with simple averaging
    """
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError(
            "SDP input {} is not an sdpsolve SDP object".format(sdp_obj.__repr__))

    if pure_state:
        projections = [solve_linear_projection, solve_positive_projection, solve_positive_projection_boreland_dennis]
    else:
        projections = [solve_linear_projection, solve_positive_projection]
    # allocate memory for python.  If we use the copyto commands this should be fast (slow still)
    # also we need this in a list because numpy bug in copyto...it won't copy data into slices of arrays
    # for example...A vector into a column of a matrix...it won't throw anything...it just doesn't do it.
    previous_vector = np.zeros((sdp_obj.nv, 1))
    iter = 0
    while iter < DYKSTRA_ITERMAX:
        for pi in range(len(projections)):
            sdp_data = projections[pi](sdp_obj, sdp_data)

        iteration_residual = np.linalg.norm(previous_vector - sdp_data.primal_vector)
        np.copyto(previous_vector, sdp_data.primal_vector)
        if disp:
            print("Bregman Iter {} convergence \t{}".format(iter, iteration_residual))
        if iteration_residual < epsilon:
            print("Bregman Iter {} convergence \t{}".format(iter, iteration_residual))
            break
        iter += 1

    print("Bregman Iter {} convergence \t{}".format(iter, iteration_residual))
    return sdp_data


def solve_alternating_projections_bregman_averaged(sdp_obj, sdp_data, epsilon=1.0E-4, pure_state=False, disp=False):
    """
    Solve projection onto positive semidefinite set intersected with an affine subspace
    with simple averaging
    """
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError(
            "SDP input {} is not an sdpsolve SDP object".format(sdp_obj.__repr__))

    if pure_state:
        projections = [solve_linear_projection, solve_positive_projection_boreland_dennis]
    else:
        projections = [solve_linear_projection, solve_positive_projection]
    # allocate memory for python.  If we use the copyto commands this should be fast (slow still)
    # also we need this in a list because numpy bug in copyto...it won't copy data into slices of arrays
    # for example...A vector into a column of a matrix...it won't throw anything...it just doesn't do it.
    previous_vector = np.zeros((sdp_obj.nv, 1))
    temp_vector = np.zeros((sdp_obj.nv, 1))
    update_vector = np.zeros((sdp_obj.nv, 1))
    iter = 0
    while iter < DYKSTRA_ITERMAX:
        np.copyto(temp_vector, sdp_data.primal_vector)
        update_vector = np.zeros((sdp_obj.nv, 1))
        for pi in range(len(projections)):
            sdp_data = projections[pi](sdp_obj, sdp_data)
            update_vector += 0.5 * sdp_data.primal_vector
            np.copyto(sdp_data.primal_vector, temp_vector)
        np.copyto(sdp_data.primal_vector, update_vector)

        iteration_residual = np.linalg.norm(previous_vector - sdp_data.primal_vector)
        np.copyto(previous_vector, sdp_data.primal_vector)
        if disp:
            print("Bregman Iter {} convergence \t{}".format(iter, iteration_residual))
        if iteration_residual < epsilon:
            print("Bregman Iter {} convergence \t{}".format(iter, iteration_residual))
            break
        iter += 1

    print("Bregman Iter {} convergence \t{}".format(iter, iteration_residual))
    return sdp_data



def solve_alternating_projections_douglas_rachford(sdp_obj, sdp_data, epsilon=0.5E-5, pure_state=False, disp=False):
    """
    Solve projection onto positive semidefinite set intersected with an affine subspace
    """
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError(
            "SDP input {} is not an sdpsolve SDP object".format(sdp_obj.__repr__))

    if pure_state:
        projections = [solve_linear_projection, solve_positive_projection_boreland_dennis]
    else:
        projections = [solve_linear_projection, solve_positive_projection]
    # allocate memory for python.  If we use the copyto commands this should be fast (slow still)
    # also we need this in a list because numpy bug in copyto...it won't copy data into slices of arrays
    # for example...A vector into a column of a matrix...it won't throw anything...it just doesn't do it.
    previous_vector = np.zeros((sdp_obj.nv, 1))
    current_vector = np.zeros((sdp_obj.nv, 1))
    reflection_temp_vector = np.zeros((sdp_obj.nv, 1))
    iter = 0
    while iter < DYKSTRA_ITERMAX:
        np.copyto(current_vector, sdp_data.primal_vector)
        for pi in range(len(projections)):
            np.copyto(reflection_temp_vector, sdp_data.primal_vector)
            sdp_data = projections[pi](sdp_obj, sdp_data)
            np.copyto(sdp_data.primal_vector, 2 * sdp_data.primal_vector - reflection_temp_vector)

        np.copyto(sdp_data.primal_vector, (current_vector + sdp_data.primal_vector) * 0.5)

        iteration_residual = np.linalg.norm(previous_vector - sdp_data.primal_vector)
        np.copyto(previous_vector, sdp_data.primal_vector)
        if disp:
            print("DR Iter {} convergence \t{}\t{}".format(iter, iteration_residual,
                                                           np.linalg.norm(sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec)))
        if iteration_residual < epsilon:
            print("DR Iter {} convergence \t{}".format(iter, iteration_residual))
            break
        iter += 1

    print("DR Iter {} convergence \t{}".format(iter, iteration_residual))
    return sdp_data


def projected_gradient(sdp_obj, sdp_data=None, alpha=0.1, beta=0.0, disp=True, pure_state=False, disp_dykstra=False):
    """
    Implement the projected gradient algorithm for SDP optimization

    :param sdp_obj:
    :param alpha: learning rate must be > 0
    :param beta: heavy ball weighting
    :return:
    """
    if alpha <= 0:
        raise ValueError("alpha must be greater than zero")
    # if beta < 0:
    #     raise ValueError("momentum parameter must be greater than zero")
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError("sdp_obj must be a sdp container object")

    residual = np.Inf
    if sdp_data is None:
        sdp_data = SdpData(sdp_obj)

    previous_iterate = np.copy(sdp_data.primal_vector)
    previous_primal_objective = sdp_obj.cvec.T @ sdp_data.primal_vector
    iteration = 0
    initial_alpha = 10
    energies = []

    while residual > sdp_obj.epsilon:
        if iteration == 0:
            np.copyto(sdp_data.primal_vector,
                      sdp_data.primal_vector - initial_alpha * sdp_obj.cvec)
        else:
            np.copyto(sdp_data.primal_vector,
                      sdp_data.primal_vector - alpha * sdp_obj.cvec +
                      beta * (previous_iterate - sdp_data.primal_vector))

        sdp_data = solve_alternating_projections_bregman(sdp_obj, sdp_data, pure_state=pure_state, disp=disp_dykstra)
        #
        # heavy ball update
        sdp_data.primal_vector += beta * (previous_iterate - sdp_data.primal_vector)

        np.copyto(previous_iterate, sdp_data.primal_vector)
        current_primal_objective = sdp_obj.cvec.T @ sdp_data.primal_vector
        residual = abs(current_primal_objective - previous_primal_objective)

        if disp:
            print(iteration, current_primal_objective,
                  current_primal_objective - previous_primal_objective, residual)

        energies.append(current_primal_objective[0, 0].real)
        previous_primal_objective = current_primal_objective
        iteration += 1

        # if residual[0, 0] < 1.0E-4:
        #     print("resetting global")
        #     global DYKSTRA_ITERMAX
        #     DYKSTRA_ITERMAX = 1.0E5# np.Inf

    return sdp_data, energies


if __name__ == "__main__":
    import os
    from sdpsolve.sdp.io_sdpsolve import readSDP
    from scipy.sparse import coo_matrix
    from sdpsolve.solvers.bpsdp import solve_bpsdp as sbpsdp
    LIBRARY_ROOT = "/Users/lobster/opt/sdpsolve/sdpsolve/"
    # LIBRARY_ROOT = "/Users/lobster/opt/sdpsolve"
    # filename = os.path.join(LIBRARY_ROOT, "sdp/theta1.dat-s.sdp")
    # filename = os.path.join(LIBRARY_ROOT, "sdp/truss1.dat-s.sdp")
    filename = os.path.join(LIBRARY_ROOT, "sdp/Hub1DN3U10_s0.5_0.5.sdp")
    # filename = os.path.join(LIBRARY_ROOT, "examples/example7/HubK1DN6U5DQG.sdp")
    blockstruct, Amatrow, Amatcol, Amatarc, bvec, cvec, nc, nv, nnz, nb = readSDP(filename)

    Amat = coo_matrix((Amatarc, (Amatrow, Amatcol)), shape=(nc, nv)).tocsr()

    sdp_obj = sdp_mod.SDP()
    sdp_obj.nc = nc
    sdp_obj.nv = nv
    sdp_obj.nnz = nnz
    sdp_obj.nb = nb
    sdp_obj.blockstruct = blockstruct
    sdp_obj.cvec = cvec.reshape((-1, 1))
    sdp_obj.bvec = bvec.reshape((-1, 1))
    sdp_obj.Amat = Amat

    sdp_obj.Initialize()
    sdp_obj.inner_solve = 'CG'
    sdp_obj.epsilon = 1.0E-6
    sdp_obj.epsilon_inner = sdp_obj.epsilon_inner / 100
    sdp_obj.iter_max = 10000
    sdp_obj.inner_iter_max = 100
    sdp_obj.admm_style = False

    alpha = 0.1
    beta = 0.25


    sdp_data = None
    sdp_data = solve_bpsdp(sdp_obj)

    # sdp_data, energies = projected_gradient(sdp_obj, sdp_data=sdp_data, alpha=alpha, beta=beta, disp=True,
    #                                         disp_dykstra=False, pure_state=False)

    # take the objective function and cvec and add a row to linear constraint enforcing
    # <C, X> = E

    obj_energy = sdp_obj.cvec.T @ sdp_data.primal_vector
    new_Amat_row = coo_matrix(sdp_obj.cvec.reshape((1, -1))).tocsr()
    new_Amat_row.eliminate_zeros()
    sdp_obj.Amat = scipy.sparse.vstack([sdp_obj.Amat, new_Amat_row])
    sdp_obj.bvec = np.vstack([sdp_obj.bvec, obj_energy])
    sdp_obj.nc = sdp_obj.Amat.shape[0]
    sdp_obj.nnz = sdp_obj.Amat.nnz

    old_vec = sdp_data.primal_vector.copy()
    sdp_data2 = solve_linear_projection(sdp_obj, sdp_data)
    sdp_data2.primal_vector = 2 * sdp_data2.primal_vector - old_vec
    print(sdp_obj.cvec.T @ sdp_data2.primal_vector)
    sdp_data3 = solve_positive_projection_boreland_dennis(sdp_obj, sdp_data)
    # sdp_data = solve_alternating_projections_douglas_rachford(sdp_obj, sdp_data, epsilon=1.0E-4, pure_state=True, disp=True)
    # print(sdp_obj.cvec.T @ sdp_data.primal_vector)
