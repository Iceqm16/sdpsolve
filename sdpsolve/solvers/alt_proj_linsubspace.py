"""
Find the projection of a vector on the spaces spanned by A and B in R^{5}
Note: Derivation of the projection operators onto a linear space

min 0.5 ||x - c||^{2} 
"""

import numpy as np
from numpy.linalg import svd
import matplotlib.pyplot as plt


def null_space(A, rcond=None):
    u, s, vh = svd(A, full_matrices=True)
    M, N = u.shape[0], vh.shape[1]
    if rcond is None:
        rcond = np.finfo(s.dtype).eps * max(M, N)
    tol = np.amax(s) * rcond
    num = np.sum(s > tol, dtype=int)
    Q = vh[num:,:].T.conj()
    return Q


def u_int_w_basis(A, B):
    return np.hstack([A, B]) @ null_space(np.hstack([A, -B]))


def altproj_n(A, B, v, n):
    if n % 2 != 0:
        raise TypeError("n must be even")

    proj_u_v = lambda z: A @ np.linalg.inv(A.T @ A) @ A.T @ z
    proj_c_v = lambda z: B @ np.linalg.inv(B.T @ B) @ B.T @ z
    for _ in range(n):
        v = proj_c_v(proj_u_v(v))
    return v


if __name__ == "__main__":
    v = np.arange(5).reshape((-1, 1))
    A = np.array([[3, 2, 3],
                  [1, 5, 7],
                  [3, 11, 13],
                  [1, 17, 19],
                  [5, 23, 29]])

    B = np.array([[1, 1, 2.5],
                  [2, 0, 6],
                  [2, 1, 12],
                  [2, 0, 18],
                  [6, -3, 26]])

    PUW = u_int_w_basis(A, B)
    proj_u_w = lambda z: PUW @ np.linalg.inv(PUW.T @ PUW) @ PUW.T @ z
    print(proj_u_w(v))

    steps = np.arange(0, 30, 2)
    vecs = [np.array(altproj_n(A, B, v, x)) for x in steps]
    diffs = [np.linalg.norm(vecs[i] - proj_u_w(v)) for i in range(len(vecs))]
    plt.semilogy(steps, diffs, 'C0o-')
    plt.show()