import numpy as np


def DIISUpdate(y, dual_list, error_list, dual_iter, DIIS_length):
    ###########################################################
    #
    #   DIIS procedure on dual variables.  Try to do better
    #           than a first order update
    #
    ##########################################################
    # print "doing DIIS"
    # print dual_iter
    if dual_iter > 0:
        if len(dual_list) < DIIS_length:
            error_list.append(y - y_old)
            dual_list.append(y)
        elif len(dual_list) >= DIIS_length:
            del error_list[0]
            del dual_list[0]
            error_list.append(y - y_old)
            dual_list.append(y)

    # print len(dual_list)
    # print len(error_list)
    if len(error_list) >= 2:
        print("Doing DIIS with {} error vectors".format(len(error_list)))
        B = np.zeros((len(error_list), len(error_list)))
        B = np.insert(B, 0, -1, axis=0)
        B = np.insert(B, 0, -1, axis=1)
        B[0, 0] = 0.0
        b = np.zeros(B.shape[-1])
        b[0] = -1
        for erri in range(len(error_list)):
            for errj in range(len(error_list)):
                B[erri + 1, errj + 1] = np.dot(error_list[erri].T,
                                               error_list[errj])

        coeffs = np.linalg.solve(B, b)
        y = np.zeros_like(y_old)
        for c, vec in zip(coeffs[1:], dual_list):
            y += c * vec

    return y


def Nesterov(y, y_tilde, y_tilde_old, tk):
    ###########################################################################
    #
    # Nesterov update step
    #
    # y_{k} = \hat{y}_{k} - \tau(Ax_{k} - b)
    # t_{k+1} = 1 + \sqrt{1+4t_{k}^{2}
    # \hat{y}_{k+1} = y_{k} + (tk - 1)/(t_{k+1}) (yk - \hat{y}_{k-1}) +
    #      (tk/t_{k+1})(\hat{y}_{k} - y_{k})
    #
    ###########################################################################

    tk1 = (1 + np.sqrt(1 + 4 * tk ** 2)) / 2.

    yk1 = y_tilde + ((tk - 1) / tk1) * (y_tilde - y_tilde_old) + (tk / tk1) * (
    y_tilde - y)

    return yk1, tk1
