'''
RRSDP code by Nicholas C Rubin
'''

# MAJOR LIBRARY IMPORTS
import time

# OPTIMIZATION ROUTINE AND SPARSE IMPORTS
from scipy.optimize import minimize

# PROJECT IMPORTS
from sdpsolve.utils.RDMUtils import *
from sdpsolve.solvers.rrsdp.multiplierupdates import *
from sdpsolve.solvers.rrsdp.lagrangian import *


def banner():
    str1 = "\n\n"
    str1 += "".join(['-'] * 50)
    str1 += "\n\n\tRRSDP by Nicholas C. Rubin\n"
    str1 += "\tVersion 0.0.1\n\n"
    str1 += "".join(["-"] * 50)
    str1 += "\n"

    print(str1)


def error_L1norm(residual):
    return np.max(np.abs(residual))


def error_L2norm2(residual):
    return np.sum(np.square(residual))


def solve_rrsdp(SDP):
    banner()

    ####################################
    #
    #  Prepare program get SDP file
    #
    ####################################

    master_start = time.time()

    blockstruct = SDP.blockstruct
    Amat = SDP.Amat
    bvec = SDP.bvec
    cvec = SDP.cvec
    nv = SDP.nv
    nc = SDP.nc
    Norm = SDP.Norm
    eta = SDP.eta
    epsilon = SDP.epsilon
    epsilon_inner = SDP.epsilon_inner
    iter_max = SDP.iter_max
    inner_solve = SDP.inner_solve
    DIIS = SDP.DIIS
    NESTEROV = SDP.NESTEROV

    if SDP.disp:
        print("\n\tNumber of Variables {}".format(nv))
        print("\tNumber of Constraints {}".format(nc))
        print("\tBlockstructure {}\n\n".format(",".join(map(str, blockstruct))))
        print("\tIMPORTED VARIABLES FROM input.py")
        print("\tNorm for residual = {}".format(Norm))
        print("\tEta for primal feasibility reduction = {:3.10E}".format(eta))
        print("\tepsilon for convergence = {:3.10E}".format(epsilon))
        print("\titer_max = {}".format(iter_max))
        print("\tinner_solve = {}".format(inner_solve))
        print("\n\n")

    #######################################################################
    #
    # Initialize factored primal and dual solution.
    # Initialize sigma, gamma
    #
    #######################################################################

    R = []
    for i in blockstruct:
        R.append(np.eye(i).flatten(order='F'))
    R = np.require(np.hstack(R), dtype=float, requirements=['F', 'A', 'W', 'O'])
    y = np.require(np.asfortranarray(np.zeros((nc, 1))), dtype=float,
                   requirements=['F', 'A', 'W', 'O'])

    sigma = 5
    gamma = np.sqrt(10)

    #######################################################################
    #
    # Start iterations
    # while (error > epsilon and outer_iter < iter_max )
    #
    #######################################################################

    # AUGMENTED LAGRANGIAN MULTIPLIER METHOD SETUP
    outer_iter = 0
    dual_iter = 0
    nu_old = 100
    dual_feasibility = 100
    dual_feasibility_old = 100
    error = 10
    # DIIS SETUP
    dual_list = []
    error_list = []
    y_old = np.copy(y)
    # NESTEROV SETUP
    tk = 1
    y_tilde = np.copy(y)
    y_tilde_old = np.copy(y)

    # SET WHICH ERROR WE WANT TO USE FOR RESIDUAL
    if Norm == "L1":
        error_f = error_L1norm
    elif Norm == "L2":
        error_f = error_L2norm2
    else:
        print
        "\n\tNorm variable not recognized. Going with L1"
        error_f = error_L1norm

    # ITERATION LOOP
    while outer_iter < iter_max and error > epsilon:

        if SDP.disp:
            print("iteration Number = {}\t".format(outer_iter))

        # res = minimize(
        #    Lagrangian_val, R, method='L-BFGS-B', jac=Lagrangian_grad, args=( y,
        #        blockstruct, cvec, Amat, bvec, sigma),)
        res = minimize(
            Lagrangian_val, R, method='BFGS', jac=Lagrangian_grad, args=(y,
                                                                         blockstruct,
                                                                         cvec,
                                                                         Amat,
                                                                         bvec,
                                                                         sigma), )

        # options={'disp':True} #for debug

        R = res['x']
        status = res['status']
        fval = res['fun']

        if status != 0:
            raise Exception("status ", status)
            sys.exit()

        tRDMs = vec2blockRR(blockstruct, R)
        Rmats4norm = vec2block(blockstruct, R)
        tvecRDMs = map(lambda x: x.reshape((x.shape[0] ** 2, 1), order='F'),
                       tRDMs)
        tvecRDMs = np.vstack(tvecRDMs)

        if isinstance(Amat, csr_matrix) or isinstance(Amat, csc_matrix):
            tresidual = Amat.dot(tvecRDMs) - bvec
            S = cvec - Amat.T.dot(y)
        else:
            tresidual = np.dot(Amat, tvecRDMs) - bvec
            S = cvec - np.dot(Amat.T, y)
        Smats = vec2block(blockstruct, S)

        eigs = []
        for mat in Smats:
            w, v = np.linalg.eigh(mat)
            eigs += list(w)
        dualval = np.dot(bvec.T, y)
        dual_feasibility = np.dot(S.reshape(nv), R)
        terror = error_f(tresidual)

        if SDP.disp:
            print("Primal Energy = %3.14f" % np.dot(cvec.T, tvecRDMs))
            print("\tDual Energy = %3.15f" % dualval)
            print("\tLagrangian energy = %3.15f" % fval)
            print("\tConstraint norm = %3.15E" % terror)
            print("\t<X,Z> = %3.15E" % dual_feasibility)
            print("\t" + "".join(["--"] * 30))
            print("\tmu  = %3.15E" % (1. / sigma))

        if status == 0:
            error = error_f(tresidual)
            nu = error

            if SDP.disp:
                if outer_iter == 0:
                    print("\teta = %3.15E" % nu)
                    print("\ttol = %3.15E" % nu)
                else:
                    print("\ttol = %3.15E" % nu)
                    print("\teta = %3.15E" % nu_old)

            if nu / nu_old < eta:
                # save old iterate and update multipliers
                y_old = y
                # reshape shouldn't need to move the data to flatten
                y = y - sigma * tresidual
                if SDP.disp:
                    print("\tupdated duals")
                dual_iter += 1

            else:

                sigma = gamma * sigma
                if SDP.disp:
                    print("\t%fx on penalty updated penalty" % (gamma))

            nu_old = nu
            dual_feasibility_old = dual_feasibility
            outer_iter += 1

        else:
            # L-BFGS optimization failed to find minimum.
            # Solution: Increase penalty while everything else stays the same
            error_list = []
            dual_list = []
            gamma2 = 0.5
            sigma = sigma * gamma2
            if SDP.disp:
                print("\tMinimum of lagrangian not found using  %s." % "L-BFGS")
                print(" Resrtarting unconstrained opt")
                print("\tpenalty parameter updated by x%f" % gamma2)

    tRDMs = vec2blockRR(blockstruct, R)
    # tRDMs = map(lambda x: x.T.dot(x), tRDMs)
    tvecRDMs = np.vstack(
        (map(lambda x: x.reshape((x.shape[0] ** 2, 1), order='F'), tRDMs)))
    SDP.primal = tvecRDMs
    SDP.dual = (y, S)

    print("\n\tTotal time = %f" % (time.time() - master_start))
