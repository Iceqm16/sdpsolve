import numpy as np
from scipy.sparse import csr_matrix, csc_matrix
from sdpsolve.utils.matreshape import *


def Lagrangian(R, y, blockstruct, cvec, Amat, bvec,
               sigma):
    # get RDMs RR^{T}
    RDMs = vec2blockRR(blockstruct, R)
    Rmats = vec2block(blockstruct, R)

    # vectorize
    vecRDMs = map(lambda x: x.reshape((x.shape[0] ** 2, 1), order='F'), RDMs)
    vecRDMs = np.vstack(vecRDMs)

    # evaluate residual
    # numpy >= 1.7 np.dot() has no knowledge of sparse matrices.  Use Amat.dot(vecRDMs)
    # when using sparse
    if isinstance(Amat, csr_matrix):
        residual = Amat.dot(vecRDMs) - bvec
        S = cvec - Amat.T.dot(y)
    else:
        residual = np.dot(Amat, vecRDMs) - bvec
        S = cvec - np.dot(Amat.T, y)

    Smats = vec2block(blockstruct, S)

    L_val = 0
    # complementary slackness contribution
    for i in range(len(Smats)):
        L_val += np.trace(np.dot(Smats[i], RDMs[i]))
        # L_val += np.einsum('ij,ji',Smats[i],RDMs[i])
    # dual contribution
    L_val += np.dot(bvec.T, y)

    # augmented piece
    L_val += (sigma / (2.)) * np.sum(np.square(residual))

    # Gradient
    ytilde = y - sigma * (residual)
    if isinstance(Amat, csr_matrix):
        Stilde = cvec - Amat.T.dot(ytilde)
    else:
        Stilde = cvec - np.dot(Amat.T, ytilde)

    Stilde = vec2block(blockstruct, Stilde)
    grad2 = []
    for i in range(len(Smats)):
        grad2.append(2 * np.dot(Stilde[i], Rmats[i]))
    grad2 = block2vec(grad2)

    return L_val, grad2


def Lagrangian_val(R, y, blockstruct, cvec, Amat, bvec,
                   sigma):
    # get RDMs RR^{T}
    RDMs = vec2blockRR(blockstruct, R)
    Rmats = vec2block(blockstruct, R)

    # vectorize
    vecRDMs = map(lambda x: x.reshape((x.shape[0] ** 2, 1), order='F'), RDMs)
    vecRDMs = np.vstack(vecRDMs)

    # evaluate residual
    # numpy >= 1.7 np.dot() has no knowledge of sparse matrices.  Use Amat.dot(vecRDMs)
    # when using sparse
    if isinstance(Amat, csr_matrix) or isinstance(Amat, csc_matrix):
        residual = Amat.dot(vecRDMs) - bvec
        S = cvec - Amat.T.dot(y)
    else:
        residual = np.dot(Amat, vecRDMs) - bvec
        S = cvec - np.dot(Amat.T, y)

    Smats = vec2block(blockstruct, S)

    L_val = 0
    # complementary slackness contribution
    for i in range(len(Smats)):
        L_val += np.trace(np.dot(Smats[i], RDMs[i]))
        # L_val += np.einsum('ij,ji',Smats[i],RDMs[i])
    # dual contribution
    L_val += np.dot(bvec.T, y)

    # augmented piece
    L_val += (sigma / (2.)) * np.sum(np.square(residual))

    return L_val


def Lagrangian_grad(R, y, blockstruct, cvec, Amat, bvec,
                    sigma):
    # get RDMs RR^{T}
    RDMs = vec2blockRR(blockstruct, R)
    Rmats = vec2block(blockstruct, R)

    # vectorize
    vecRDMs = map(lambda x: x.reshape((x.shape[0] ** 2, 1), order='F'), RDMs)
    vecRDMs = np.vstack(vecRDMs)

    # evaluate residual
    # numpy >= 1.7 np.dot() has no knowledge of sparse matrices.  Use Amat.dot(vecRDMs)
    # when using sparse
    if isinstance(Amat, csr_matrix) or isinstance(Amat, csc_matrix):
        residual = Amat.dot(vecRDMs) - bvec
        # S = cvec - Amat.T.dot(y)
    else:
        residual = np.dot(Amat, vecRDMs) - bvec
        # S = cvec - np.dot(Amat.T, y)

    # Smats = vec2block(blockstruct, S)

    # Gradient
    ytilde = y - sigma * (residual)
    if isinstance(Amat, csr_matrix) or isinstance(Amat, csc_matrix):
        Stilde = cvec - Amat.T.dot(ytilde)
    else:
        Stilde = cvec - np.dot(Amat.T, ytilde)

    Stilde = vec2block(blockstruct, Stilde)
    grad2 = []
    for i in range(len(Stilde)):
        grad2.append(2 * np.dot(Stilde[i], Rmats[i]))
    grad2 = block2vec(grad2)

    return grad2.flatten()
