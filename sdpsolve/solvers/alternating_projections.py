"""
Projected gradient algorithm with heavy ball weighting as an option

also allows for pure-state projection

Author: Nicholas C Rubin
"""

# OPTIMIZATION ROUTINE AND SPARSE IMPORTS
import scipy
from scipy.sparse.linalg import cg as cg_solve
from scipy.sparse.linalg import inv as sp_sparse_inv
from scipy.sparse import csc_matrix

# PROJECT IMPORTS
from sdpsolve.utils.RDMUtils import *
from sdpsolve.utils.matreshape import *
from sdpsolve.sdp import sdp as sdp_mod
import numpy as np

from collections import deque
import cvxpy as cvx

from sdpsolve.solvers.bpsdp2 import solve_bpsdp

DYKSTRA_ITERMAX = 10000 # np.Inf

class ConvergenceWarning(Warning):
    pass


class SdpData(object):
    def __init__(self, sdp_obj):
        self.sdp_obj = sdp_obj
        self.primal_blocks = []
        for i in sdp_obj.blockstruct:
            self.primal_blocks.append(np.eye(i).flatten(order='F'))
        self.primal_vector = np.require(np.hstack(self.primal_blocks), dtype=float, requirements=['F', 'A', 'W', 'O']).reshape((-1, 1))
        self.dual_y = np.require(np.asfortranarray(np.zeros((sdp_obj.nc, 1))), dtype=float,
                       requirements=['F', 'A', 'W', 'O'])

        self.sigma = 5
        self.mu = 1. / self.sigma
        self.gamma = 10  # np.sqrt(10)

        self.z_vector = -1 * np.copy(sdp_obj.cvec).reshape((-1, 1))
        self.z_blocks = vec2block(sdp_obj.blockstruct, self.z_vector)
        self.c_blocks = vec2block(sdp_obj.blockstruct, sdp_obj.cvec)
        self.AAt = sdp_obj.Amat @ sdp_obj.Amat.T

    def residual(self):
        return self.sdp_obj.bvec - self.sdp_obj.Amat @ self.primal_vector

    def dual_residual(self):
        return self.sdp_obj.Amat.T @ self.dual_y - self.sdp_obj.cvec + self.z_vector

    def primal_value(self):
        return self.sdp_obj.cvec.T @ self.primal_vector

    def dual_value(self):
        return self.sdp_obj.bvec.T @ self.dual_y

    def gap(self):
        pv = self.primal_value()
        dv = self.dual_value()
        return abs(pv - dv) / (1 + abs(pv) + abs(dv))


def solve_linear_projection(sdp_obj, sdp_data):
    """
    Solve the projection of the current vector onto the set of linnear constraints

    min 0.5 ||x - z||^{2} s.t. Ax = b
    z is the matrix to project

    get equations Ax = b and x - z + A^{T}y = 0
    Ax - Az + AA^{T}y = 0
    b - Az + AA^{T} y = 0
    AA^{T}y = Az - b
    solve AA^{T} y = Az - b via CG
    x = z - A^{T}(y)



    :param sdp_obj:
    :param sdp_data:
    :return:
    """
    # we might have faster convergence if we seed the CG but we also get numerical
    # instabilities for some reason...!?
    y, status = cg_solve(sdp_obj.Amat @ sdp_obj.Amat.T,
                         sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec,
                         # x0=sdp_data.dual_y,
                         tol=sdp_obj.epsilon_inner)
    if status != 0:
        raise ValueError("CG Solve didn't work")  # I know...throw a real error

    sdp_data.dual_y = np.copy(y.reshape((-1, 1)))
    # sdp_data.dual_y = sp_sparse_inv(sdp_data.AAt) @ (sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec)

    # sdp_data.primal_vector = sdp_data.primal_vector - sdp_obj.Amat.T @ sdp_data.dual_y
    sdp_data.primal_vector -= sdp_obj.Amat.T @ sdp_data.dual_y
    # print("CG Status ", status)
    # if np.linalg.norm(sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec) > sdp_obj.epsilon_inner:
    #     print(np.linalg.norm(sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec))
    #     print(np.linalg.norm(sdp_obj.Amat @ sdp_obj.Amat.T @ sdp_data.dual_y - (sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec)))
    #     print(sdp_obj.epsilon_inner)
    #     raise ValueError("something is wrong with our solution to the linear system")

    return sdp_data


def solve_positive_projection(sdp_obj, sdp_data):
    """
    Solve the positive projection problem

    min 0.5||x - z||^{2} s.t. x >= 0

    x is positive semidefinite

    note: need to turn the vector into blocks first then back to vector
    :param sdp_data:
    :return:
    """
    sdp_data.primal_blocks = vec2block(sdp_obj.blockstruct, sdp_data.primal_vector, order='C')
    for idx_rdm, rdm in enumerate(sdp_data.primal_blocks):
        w, v = np.linalg.eigh(rdm)
        idxp = np.where(w >= -float(1.0E-14))[0]
        positive_projection = v[:, idxp] @ np.diag(w[idxp]) @ v[:, idxp].T
        sdp_data.primal_blocks[idx_rdm] = positive_projection

    sdp_data.primal_vector = block2vec(sdp_data.primal_blocks)
    return sdp_data


def opdm_projection_boreland_dennis(opdms, epsilon=1.0E-8, verbose=False):
    """
    project the opdms to pure state

    :param opdms: list of opdms.  if length 1 then just spin-orbital 1-RDM.
                  if length 2 then alpha-block is first then beta-block
    :return: projected RDMs in a similar list as the input
    """
    if len(opdms) == 2:
       w_opdm_0, v_opdm_0 = np.linalg.eigh(opdms[0])
       w_opdm_1, v_opdm_1 = np.linalg.eigh(opdms[1])

       # now sort eigvals and eig vecs in descending order
       order = np.argsort(np.hstack((w_opdm_0, w_opdm_1)))[::-1]  # descending order
       w_opdm = np.hstack((w_opdm_0, w_opdm_1))[order]
       v_opdm = np.hstack((np.vstack((v_opdm_0, np.zeros_like(v_opdm_0))), np.vstack((np.zeros_like(v_opdm_1), v_opdm_1))))
       v_opdm = v_opdm[:, order]
    elif len(opdms) == 1:
        w_opdm, v_opdm = np.linalg.eigh(opdms[0])
    else:
        raise TypeError("list for 1-RDM index should be length 1 or 2")

    # Boreland-Dennis constraints
    # λ3 − λ4 − λ5 ≤ 0
    # λ0 + λ5 = 1
    # λ1 + λ4 = 1
    # λ2 + λ3 = 1
    # print(w_opdm[3] - w_opdm[4] - w_opdm[5])
    # print(w_opdm[0] + w_opdm[5])
    # print(w_opdm[1] + w_opdm[4])
    # print(w_opdm[2] + w_opdm[3])

    # solve the system to get the eigenvalue projection to closest pure state 1-RDM
    dim = 6
    trace_target = 3  # Boreland-Dennis constraint for ^3 H
    cx = cvx.Variable(dim)
    constraints = [cx >= 0, cx <= 1, cvx.sum(cx) == trace_target,
                   cx[3] - cx[4] - cx[5] <= 0,
                   cx[0] + cx[5] == 1,
                   cx[1] + cx[4] == 1,
                   cx[2] + cx[3] == 1]
    objective = cvx.Minimize(cvx.sum_squares(cx - w_opdm))
    problem = cvx.Problem(objective, constraints)
    problem.solve(solver='SCS', verbose=verbose, eps=epsilon)

    if len(opdms) == 2:
        # generate new 1-RDM in the original ordering
        new_opdm = v_opdm[:, order] @ np.diag(cx.value[order]) @ v_opdm[:, order].T
        new_opdm_0 = new_opdm[:3, :3]
        new_opdm_1 = new_opdm[3:, 3:]
        opdms[0] = new_opdm_0
        opdms[1] = new_opdm_1
    else:
        new_opdm = v_opdm[:, ::-1] @ np.diag(cx.value[::-1]) @ v_opdm[:, ::-1].T
        opdms[0] = new_opdm

    return opdms


def solve_positive_projection_boreland_dennis_fixed_energy(sdp_obj, sdp_data, opdm_idx=[0]):
    """
    Solve the positive projection problem

    min 0.5 ||x - z||^{2} s.t. x >= 0 lambda(x) in pure state

    :param sdp_obj:
    :param sdp_data:
    :param opdm_idx: indices of the 1-RDMs in the blockstruct
    :return:
    """
    rdms = vec2block(sdp_obj.blockstruct, sdp_data.primal_vector)
    cblocks = vec2block(sdp_obj.blockstruct, sdp_obj.cvec)

    if len(opdm_idx) == 2:
        opdms = opdm_projection_boreland_dennis_fixed_energy([rdms[opdm_idx[0]], rdms[opdm_idx[1]]],
                                                             [cblocks[opdm_idx[0]], cblocks[opdm_idx[1]]],
                                                             verbose=True)
        rdms[opdm_idx[0]] = opdms[0]
        rdms[opdm_idx[1]] = opdms[1]
    elif len(opdm_idx) == 1:
        opdms = opdm_projection_boreland_dennis([rdms[opdm_idx[0]]])
        rdms[opdm_idx[0]] = opdms[0]
    else:
        raise TypeError("opdm_idx should be length 1 or 2")


    for idx, rdm in enumerate(rdms):
        if idx not in opdm_idx:
            w, v = np.linalg.eigh(rdm)
            idxp = np.where(w >= -float(1.0E-14))[0]
            positive_projection = v[:, idxp] @ np.diag(w[idxp]) @ v[:, idxp].T
            rdms[idx] = positive_projection

    sdp_data.primal_blocks = rdms
    sdp_data.primal_vector = block2vec(rdms)
    return sdp_data


def solve_positive_projection_boreland_dennis(sdp_obj, sdp_data, opdm_idx=[0]):
    """
    Solve the positive projection problem

    min 0.5 ||x - z||^{2} s.t. x >= 0 lambda(x) in pure state

    :param sdp_obj:
    :param sdp_data:
    :param opdm_idx: indices of the 1-RDMs in the blockstruct
    :return:
    """
    rdms = vec2block(sdp_obj.blockstruct, sdp_data.primal_vector)

    if len(opdm_idx) == 2:
        opdms = opdm_projection_boreland_dennis([rdms[opdm_idx[0]], rdms[opdm_idx[1]]])

        rdms[opdm_idx[0]] = opdms[0]
        rdms[opdm_idx[1]] = opdms[1]
    elif len(opdm_idx) == 1:
        opdms = opdm_projection_boreland_dennis([rdms[opdm_idx[0]]])
        rdms[opdm_idx[0]] = opdms[0]
    else:
        raise TypeError("opdm_idx should be length 1 or 2")


    for idx, rdm in enumerate(rdms):
        if idx not in opdm_idx:
            w, v = np.linalg.eigh(rdm)
            idxp = np.where(w >= -float(1.0E-14))[0]
            positive_projection = v[:, idxp] @ np.diag(w[idxp]) @ v[:, idxp].T
            rdms[idx] = positive_projection

    sdp_data.primal_blocks = rdms
    sdp_data.primal_vector = block2vec(rdms)
    return sdp_data


def solve_alternating_projections2(sdp_obj, sdp_data, epsilon=1.0E-8, pure_state=False, disp=False):
    """
    Solve projection onto positive semidefinite set intersected with an affine subspace
    using Dykstra's algorithm
    """
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError(
            "SDP input {} is not an sdpsolve SDP object".format(sdp_obj.__repr__))

    if pure_state:
        projections = [solve_linear_projection, solve_positive_projection_boreland_dennis]
    else:
        projections = [solve_positive_projection, solve_linear_projection]
    # allocate memory for python.  If we use the copyto commands this should be fast (slow still)
    # also we need this in a list because numpy bug in copyto...it won't copy data into slices of arrays
    # for example...A vector into a column of a matrix...it won't throw anything...it just doesn't do it.
    corrections = [np.zeros((sdp_obj.nv, 1)) for _ in range(len(projections))]
    subtracted_increment_vector = np.zeros((sdp_obj.nv, 1))
    previous_vector = np.zeros((sdp_obj.nv, 1))
    iter = 0
    while iter < DYKSTRA_ITERMAX:
        corrections_residual = 0.0
        for pi in range(len(projections)):
            # update sdp_data
            print(projections[pi].__name__)
            print(np.linalg.norm(sdp_data.residual()))
            sdp_data.primal_vector += corrections[pi]
            y_sdp_data = projections[pi](sdp_obj, sdp_data)
            corrections[pi] = sdp_data.primal_vector - y_sdp_data.primal_vector
            sdp_data = y_sdp_data
            print(np.linalg.norm(sdp_data.residual()))

        iteration_residual = np.linalg.norm(previous_vector - sdp_data.primal_vector)
        np.copyto(previous_vector, sdp_data.primal_vector)
        if disp and iter % 500 == 0:
            linear_norm = np.linalg.norm(sdp_data.residual())
            corrections_residual += np.linalg.norm(corrections)
            print("Dykstra's Iter {} convergence {}\t{}\t{}".format(iter, corrections_residual, iteration_residual, linear_norm))
        if max(corrections_residual, iteration_residual) < epsilon:
            print("Dykstra's Iter {} convergence {}\t{}".format(iter, corrections_residual, iteration_residual))
            break
        iter += 1

    print("Dykstra's Iter {} convergence {}\t{}".format(iter, corrections_residual, iteration_residual))
    return sdp_data




def solve_alternating_projections(sdp_obj, sdp_data, epsilon=1.0E-8, pure_state=False, disp=False):
    """
    Solve projection onto positive semidefinite set intersected with an affine subspace
    using Dykstra's algorithm
    """
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError(
            "SDP input {} is not an sdpsolve SDP object".format(sdp_obj.__repr__))

    if pure_state:
        projections = [solve_linear_projection, solve_positive_projection_boreland_dennis]
    else:
        projections = [solve_linear_projection, solve_positive_projection]
    # allocate memory for python.  If we use the copyto commands this should be fast (slow still)
    # also we need this in a list because numpy bug in copyto...it won't copy data into slices of arrays
    # for example...A vector into a column of a matrix...it won't throw anything...it just doesn't do it.
    corrections = [np.zeros((sdp_obj.nv, 1)) for _ in range(len(projections))]
    subtracted_increment_vector = np.zeros((sdp_obj.nv, 1))
    previous_vector = np.zeros((sdp_obj.nv, 1))
    iter = 0
    while iter < DYKSTRA_ITERMAX:
        corrections_residual = 0.0
        for pi in range(len(projections)):
            # first make x_{i - 1}^{k} - y_{i}^{k - 1}
            np.copyto(subtracted_increment_vector,
                      sdp_data.primal_vector - corrections[pi], casting='no')
            # second copy the vector into the vector to project and pass to the projection
            np.copyto(sdp_data.primal_vector, subtracted_increment_vector)
            sdp_data = projections[pi](sdp_obj, sdp_data)

            # calculate Ci residual component
            # remember that y_{i} = x_{i} - (x_{i-1}^{k} - y_{i}^{k - 1})
            corrections_residual += np.linalg.norm(corrections[pi] - sdp_data.primal_vector + subtracted_increment_vector)
            # third update the increment for the next round
            np.copyto(corrections[pi],
                      sdp_data.primal_vector - subtracted_increment_vector, casting='no')

        iteration_residual = np.linalg.norm(previous_vector - sdp_data.primal_vector)
        np.copyto(previous_vector, sdp_data.primal_vector)
        if disp and iter % 500 == 0:
            linear_norm = np.linalg.norm(sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec)
            print("Dykstra's Iter {} convergence {}\t{}\t{}".format(iter, corrections_residual, iteration_residual, linear_norm))
        if max(corrections_residual, iteration_residual) < epsilon:
            print("Dykstra's Iter {} convergence {}\t{}".format(iter, corrections_residual, iteration_residual))
            break
        iter += 1

    print("Dykstra's Iter {} convergence {}\t{}".format(iter, corrections_residual, iteration_residual))
    return sdp_data


def projected_gradient(sdp_obj, sdp_data=None, alpha=0.1, beta=0.0, disp=True, pure_state=False, disp_dykstra=False):
    """
    Implement the projected gradient algorithm for SDP optimization

    :param sdp_obj:
    :param alpha: learning rate must be > 0
    :param beta: heavy ball weighting
    :return:
    """
    if alpha <= 0:
        raise ValueError("alpha must be greater than zero")
    # if beta < 0:
    #     raise ValueError("momentum parameter must be greater than zero")
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError("sdp_obj must be a sdp container object")

    residual = np.Inf
    if sdp_data is None:
        sdp_data = SdpData(sdp_obj)

    previous_iterate = np.copy(sdp_data.primal_vector)
    previous_primal_objective = sdp_obj.cvec.T @ sdp_data.primal_vector
    iteration = 0
    initial_alpha = 10
    energies = []

    while residual > sdp_obj.epsilon:
        if iteration == 0:
            np.copyto(sdp_data.primal_vector,
                      sdp_data.primal_vector - initial_alpha * sdp_obj.cvec)
        else:
            np.copyto(sdp_data.primal_vector,
                      sdp_data.primal_vector - alpha * sdp_obj.cvec +
                      beta * (previous_iterate - sdp_data.primal_vector))

        sdp_data = solve_alternating_projections(sdp_obj, sdp_data, pure_state=pure_state, disp=disp_dykstra)

        # heavy ball update
        sdp_data.primal_vector += beta * (previous_iterate - sdp_data.primal_vector)

        np.copyto(previous_iterate, sdp_data.primal_vector)
        current_primal_objective = sdp_obj.cvec.T @ sdp_data.primal_vector
        residual = abs(current_primal_objective - previous_primal_objective)

        if disp:
            print(iteration, current_primal_objective,
                  current_primal_objective - previous_primal_objective, residual)

        energies.append(current_primal_objective[0, 0].real)
        previous_primal_objective = current_primal_objective
        iteration += 1

        # if residual[0, 0] < 1.0E-4:
        #     print("resetting global")
        #     global DYKSTRA_ITERMAX
        #     DYKSTRA_ITERMAX = 1.0E5# np.Inf

    return sdp_data, energies


def projected_gradient_diis(sdp_obj, alpha, subspace_size=3, disp=True):
    """
    Implement the projected gradient algorithm for SDP optimization

    :param sdp_obj:
    :param alpha: learning rate must be > 0
    :param beta: heavy ball weighting
    :return:
    """
    if alpha <= 0:
        raise ValueError("alpha must be greater than zero")
    # if beta < 0:
    #     raise ValueError("momentum parameter must be greater than zero")
    if not isinstance(sdp_obj, sdp_mod.SDP):
        raise TypeError("sdp_obj must be a sdp container object")

    residual = np.Inf
    sdp_data = SdpData(sdp_obj)
    previous_iterate = np.copy(sdp_data.primal_vector)
    previous_primal_objective = sdp_obj.cvec.T @ sdp_data.primal_vector
    iteration = 0
    initial_alpha = 1
    energies = []

    # needed for DIIS
    error_vectors = deque()
    guess_vectors = deque()

    while residual > sdp_obj.epsilon:
        np.copyto(sdp_data.primal_vector,
                  sdp_data.primal_vector - initial_alpha * sdp_obj.cvec)

        sdp_data = solve_alternating_projections(sdp_obj, sdp_data)

        residual_vec = previous_iterate - sdp_data.primal_vector  # allocate new memory every time
        residual = np.linalg.norm(residual_vec)

        # We've collected enough DIIS vectors so now we can calculate the corrected update
        if len(error_vectors) == subspace_size:
            print("DIIS ACTIVATE")
            error_vectors.popleft()  # let the GC clean up the old vector
            error_vectors.append(residual_vec)

            guess_vectors.popleft()
            guess_vectors.append(sdp_data.primal_vector)

            sdp_data.primal_vector = diis_update(error_vectors, guess_vectors)
            sdp_data = solve_alternating_projections(sdp_obj, sdp_data)

        # we haven't collected enough DIIS vectors
        else:
            if iteration not in [0, 1]:  # don't include the first vector...it's usually bad
                error_vectors.append(residual_vec)
                guess_vectors.append(sdp_data.primal_vector)

        np.copyto(previous_iterate, sdp_data.primal_vector)
        current_primal_objective = sdp_obj.cvec.T @ sdp_data.primal_vector

        if disp:
            print(iteration, current_primal_objective,
                  current_primal_objective - previous_primal_objective, residual)

        energies.append(current_primal_objective[0, 0].real)
        previous_primal_objective = current_primal_objective
        iteration += 1

    return sdp_data, energies


def diis_update(error_vectors, guess_vectors):
    """
    perform the least squares update that minimizes the error vector
    and constriants its sum to 1

    :param error_vectors:
    :param guess_vectors:
    :return:
    """
    # build B matrix
    b_matrix = np.zeros((len(error_vectors), len(error_vectors)))
    for i, j in product(range(len(error_vectors)), repeat=2):
        if i < j:  # fill out lower triangle
            b_matrix[i, j] = error_vectors[i].T @ error_vectors[j]
            b_matrix[j, i] = b_matrix[i, j]
        elif i == j:
             b_matrix[i, j] = error_vectors[i].T @ error_vectors[j]
        else:
            pass

    # build Lagrange system
    A = np.hstack((b_matrix, -1 * np.ones((len(error_vectors), 1))))
    A = np.vstack((A, np.hstack((-1 * np.ones((1, len(error_vectors))), np.array([[0]])))))
    b = np.zeros((len(error_vectors) + 1, 1))
    b[-1, 0] = -1

    # solve Ac = b to get c
    c = np.linalg.inv(A) @ b
    assert np.isclose(np.sum(c[:-1]), 1.0)
    new_vector = np.zeros_like(guess_vectors[0])
    for ii in range(len(guess_vectors)):
        new_vector += c[ii] * guess_vectors[ii]

    return new_vector


if __name__ == "__main__":
    import os
    from sdpsolve.sdp.io_sdpsolve import readSDP
    from scipy.sparse import coo_matrix
    from sdpsolve.solvers.bpsdp import solve_bpsdp as sbpsdp
    LIBRARY_ROOT = "/Users/lobster/opt/sdpsolve/sdpsolve/"
    # LIBRARY_ROOT = "/Users/lobster/opt/sdpsolve"
    # filename = os.path.join(LIBRARY_ROOT, "sdp/theta1.dat-s.sdp")
    # filename = os.path.join(LIBRARY_ROOT, "sdp/truss1.dat-s.sdp")
    filename = os.path.join(LIBRARY_ROOT, "sdp/Hub1DN3U10_s0.5_0.5.sdp")
    # filename = os.path.join(LIBRARY_ROOT, "examples/example7/HubK1DN6U5DQG.sdp")
    blockstruct, Amatrow, Amatcol, Amatarc, bvec, cvec, nc, nv, nnz, nb = readSDP(filename)
    print(blockstruct)

    Amat = coo_matrix((Amatarc, (Amatrow, Amatcol)), shape=(nc, nv)).tocsr()

    sdp_obj = sdp_mod.SDP()
    sdp_obj.nc = nc
    sdp_obj.nv = nv
    sdp_obj.nnz = nnz
    sdp_obj.nb = nb
    sdp_obj.blockstruct = blockstruct
    sdp_obj.cvec = cvec.reshape((-1, 1))
    sdp_obj.bvec = bvec.reshape((-1, 1))
    sdp_obj.Amat = Amat

    sdp_obj.Initialize()
    sdp_obj.inner_solve = 'CG'
    sdp_obj.epsilon = 1.0E-6
    sdp_obj.epsilon_inner = sdp_obj.epsilon_inner / 100
    sdp_obj.iter_max = 10000
    sdp_obj.inner_iter_max = 100
    sdp_obj.admm_style = False

    alpha = 0.1
    beta = 0.25

    sdp_data = None
    sdp_data = solve_bpsdp(sdp_obj)
    # sdp_data, energies = projected_gradient(sdp_obj, sdp_data=sdp_data, alpha=alpha, beta=beta, disp=True,
    #                                         disp_dykstra=False, pure_state=True)

    obj_energy = sdp_obj.cvec.T @ sdp_data.primal_vector
    new_Amat_row = coo_matrix(sdp_obj.cvec.reshape((1, -1))).tocsr()
    new_Amat_row.eliminate_zeros()
    sdp_obj.Amat = scipy.sparse.vstack([sdp_obj.Amat, 100 * new_Amat_row])
    sdp_obj.bvec = np.vstack([sdp_obj.bvec, 100 * obj_energy])
    sdp_obj.nc = sdp_obj.Amat.shape[0]
    sdp_obj.nnz = sdp_obj.Amat.nnz

    sdp_data = solve_alternating_projections(sdp_obj, sdp_data, epsilon=1.0E-6, pure_state=True, disp=True)
    # sdp_data = solve_positive_projection_boreland_dennis(sdp_obj, sdp_data)
    # sdp_data = solve_linear_projection(sdp_obj, sdp_data)

    print(sdp_obj.cvec.T @ sdp_data.primal_vector)
    print(np.linalg.norm(sdp_obj.Amat @ sdp_data.primal_vector - sdp_obj.bvec))
    sdp_data.primal_blocks = vec2block(sdp_obj.blockstruct, sdp_data.primal_vector)

    new_opdm_a = sdp_data.primal_blocks[0]
    new_opdm_b = sdp_data.primal_blocks[1]

    new_opdm = np.zeros((6, 6))
    new_opdm[::2, ::2] = new_opdm_a
    new_opdm[1::2, 1::2] = new_opdm_b
    w_opdm, v_opdm = np.linalg.eigh(new_opdm)
    idx = np.argsort(w_opdm)[::-1]
    w_opdm = w_opdm[idx]
    v_opdm = v_opdm[:, idx]
    print(w_opdm[3] - w_opdm[4] - w_opdm[5])
    print(w_opdm[0] + w_opdm[5])
    print(w_opdm[1] + w_opdm[4])
    print(w_opdm[2] + w_opdm[3])


