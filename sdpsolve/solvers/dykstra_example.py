"""
Dykstra Alternating projections example
C1 = y >=0
C2 = x^{2} + (y - \sqrt{3}/2)^{2} <= 1
"""
import numpy as np
import matplotlib.pyplot as plt


def proj_c1(v):
    if v[1] < 0:
        v[1] = 0
        return v
    else:
        return v


def proj_c2(v):
    """
    This is harder... projection onto the circle is the following minimization

    if satisfies v[0]**2 + (v[1] + np.sqrt(3)/2)**2 <= 1 then leave the same.
    else: project to the boundary v[0]**2 + (v[1] + np.sqrt(3)/2)**2 == 1

    Solve by noticing that shifted ball projection is ball projection shifted.

    :param v:
    :return:
    """

    if v[0]**2 + (v[1] - np.sqrt(3) / 2)**2 <= 1:
        return v
    else:
        v0 = np.array([0, np.sqrt(3)/2])
        v = proj_l2_ball(v - v0) + v0
        return v


def proj_l2_ball(v):
    if np.linalg.norm(v) <= 1:
        return v
    else:
        return v / np.linalg.norm(v)


def dykstras(v, n):
    w = np.zeros((2, 2))

    v_points = [v]
    for _ in range(n):
        v_new = proj_c1(v - w[0, :])
        w[0, :] = v_new - (v - w[0, :])
        v = v_new
        print(v)

        v_new = proj_c2(v - w[1, :])
        w[1, :] = v_new - (v - w[1, :])
        v = v_new
        print(v)
        v_points.append(v)
    return v, v_points


if __name__ == "__main__":
    v0 = np.array([1, -2])
    n = 2
    v_points = [v0]
    v = np.copy(v0)
    for _ in range(n):
        v = proj_c1(v)
        print(v)
        v = proj_c2(v)
        print(v)
        v_points.append(v)

    # dykstra's alg
    # v_{i+1} = P_{C_{\sigma(i)}}(v_{i} - w_{\sigma(i)})
    # w_{\sigma(i)} = v_{i+1} - (v_{i} - w_{\sigma(i)})
    # w_{\sigma(i)} = 0 forall i in 1,...,m
    vd, vd_points = dykstras(v0, 10)

    x = np.linspace(-1, 1, 1000)
    yp = np.sqrt(1 - x**2) + np.sqrt(3)/2
    ym = -np.sqrt(1 - x**2) + np.sqrt(3)/2
    plt.plot(x, yp, 'C0-')
    plt.plot(x, ym, 'C0-')
    plt.hlines(0, -1, 1)

    plt.plot(list(map(lambda x: x[0], v_points)), list(map(lambda x: x[1], v_points)), 'C1o-')
    plt.plot(list(map(lambda x: x[0], vd_points)), list(map(lambda x: x[1], vd_points)),
            'C2o--', mec='C2', mfc='None')
    plt.show()
