import numpy as np
import matplotlib.pyplot as plt


def kaczmarz(A, b, n):
    v = np.zeros(A.shape[1])
    for _ in range(n):
        for i in range(A.shape[0]):
            v = v - ((A[i, :] @ v - b[i]) / (A[i, :] @ A[i, :])) * A[i, :]

    return v


if __name__ == "__main__":
    # A = np.array([[2, 5, 11, 17, 23],
    #               [3, 7, 13, 19, 29]])
    # b = np.array([[228],
    #               [277]])
    # v = kaczmarz(A, b, 1000)
    # print(v.reshape((-1, 1)))
    # lstsq_sol = np.linalg.lstsq(A, b, rcond=None)[0]
    # print(np.linalg.norm(v.reshape((-1, 1)) - lstsq_sol))

    # steps = np.arange(0, 5000, 100)
    # vecs = [np.array(kaczmarz(A, b, x)) for x in steps]
    # diffs = [np.linalg.norm(vecs[i].reshape((-1, 1)) - lstsq_sol) for i in range(len(vecs))]
    # plt.semilogy(steps, diffs, 'C0o-')
    # plt.show()

    A = np.random.randn(500, 1000)
    b = A @ np.random.randn(1000)
    lstsq_sol = np.linalg.lstsq(A, b, rcond=None)[0]

    steps = np.arange(0, 500, 50)
    vecs = [np.array(kaczmarz(A, b, x)) for x in steps]
    diffs = [np.linalg.norm(vecs[i] - lstsq_sol) for i in range(len(vecs))]
    plt.semilogy(steps, diffs, 'C0o-')
    plt.show()

