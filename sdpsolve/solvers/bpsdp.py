'''
BPSDP algorithm

Author: Nicholas C Rubin
Paper: Computing 78(3): 277-286 November 2006
'''

# MAJOR LIBRARY IMPORTS
import scipy as sp
import time

# OPTIMIZATION ROUTINE AND SPARSE IMPORTS
# from scipy.sparse import csr_matrix, coo_matrix, csc_matrix
from scipy.sparse.linalg import cg as cg_solve, inv
from scipy.sparse.linalg import bicgstab

# PROJECT IMPORTS
from sdpsolve.utils.RDMUtils import *
from sdpsolve.utils.matreshape import *
from sdpsolve.sdp import sdp as sdp_mod


class ConvergenceWarning(Warning):
    pass


def yzupdateExact_admm_style(R, Rmat, Amat, AmatT, cvec, zvec, Zmat, res, mu, blockstruct,
                  bvec, LowerCho, lower_bool):
    # (EXACT SOLUTION TO INNER PROBLEM)
    rhs = Amat.dot(cvec - zvec) + mu * res
    y = sp.linalg.cho_solve((LowerCho, lower_bool),
                            rhs)  # solves AAt y = A(Z+C) + (A(X) - b)/sigma

    # form w(y, X, sigma)
    W = mu * R + (AmatT).dot(y) - cvec
    Wblock = vec2block(blockstruct, W)

    # compute projection onto PSD and ND sets for each block
    for ii in range(len(blockstruct)):
        # decompose Wblock
        eiglam, eigvec = np.linalg.eigh(Wblock[ii])
        # set up matrices
        Wp = np.zeros_like(Wblock[ii]);
        Wm = np.zeros_like(Wblock[ii])
        idxp = np.where(eiglam >= 0)[0]  # get indices of eigs >= zero
        idxm = np.where(eiglam < 0)[0]  # get indices of eigs < zero
        # projection
        Wp = np.dot(eigvec[:, idxp],
                    np.dot(np.diag(eiglam[idxp]), eigvec[:, idxp].T))
        Wm = np.dot(eigvec[:, idxm],
                    np.dot(np.diag(eiglam[idxm]), eigvec[:, idxm].T))

        # update V vector
        Rmat[ii] = Wp / mu
        Zmat[ii] = -Wm

    R = block2vec(Rmat)
    zvec = block2vec(Zmat)
    res = bvec - Amat.dot(R)
    dres = (AmatT).dot(y) - cvec + zvec

    return R, zvec, res, dres, y


def yzupdateExact(R, Rmat, Amat, AmatT, cvec, zvec, Zmat, res, mu, blockstruct, epsilon_inner,
                  bvec, LowerCho, lower_bool):
    iter_max = 100
    iterations = 0
    tau = 1.6

    while iterations < iter_max:
        # (EXACT SOLUTION TO INNER PROBLEM)
        rhs = Amat.dot(cvec - zvec) + mu * res
        y = sp.linalg.cho_solve((LowerCho, lower_bool),
                                rhs)  # solves AAt y = A(c - z) + (b - A(X))/sigma

        # form w(y, X, sigma)
        W = mu * R + (AmatT).dot(y) - cvec
        Wblock = vec2block(blockstruct, W)

        # compute projection onto PSD and ND sets for each block
        for ii in range(len(blockstruct)):
            # decompose Wblock
            eiglam, eigvec = np.linalg.eigh(Wblock[ii])
            # set up matrices
            idxp = np.where(eiglam >= 0)[0]  # get indices of eigs >= zero
            idxm = np.where(eiglam < 0)[0]  # get indices of eigs < zero
            # projection
            Wp = np.dot(eigvec[:, idxp],
                        np.dot(np.diag(eiglam[idxp]), eigvec[:, idxp].T))
            Wm = np.dot(eigvec[:, idxm],
                        np.dot(np.diag(eiglam[idxm]), eigvec[:, idxm].T))

            # update V vector
            Rmat[ii] = Wp / mu
            Zmat[ii] = -Wm

        R = block2vec(Rmat)
        zvec = block2vec(Zmat)
        res = bvec - Amat.dot(R)
        dres = (AmatT).dot(y) - cvec + zvec

        if np.linalg.norm(res) < epsilon_inner:
            break

    return R, zvec, res, dres, y


def yzupdateCG_admm_style(R, Rmat, Amat, AmatT, AAt, cvec, zvec, Zmat, res, mu,
                          epsilon_inner, y, blockstruct, bvec):
    tau = 1.6
    rhs = Amat.dot(cvec - zvec) + tau * mu * res
    y, status = cg_solve(AAt, rhs, tol=epsilon_inner, x0=y)
    y = y.reshape((-1, 1))

    if status != 0:
        raise Exception("CG ERROR: NO CONVERGENCE")

    # form w(y, X, sigma)
    W = mu * R + (AmatT).dot(y) - cvec
    Wblock = vec2block(blockstruct, W)

    # compute projection onto PSD and NSD sets for each block
    for ii in range(len(blockstruct)):
        # decompose Wblock
        eiglam, eigvec = np.linalg.eigh(Wblock[ii])
        idxp = np.where(eiglam >= 0)[0]  # get indices of eigs >= zero
        idxm = np.where(eiglam < 0)[0]  # get indices of eigs < zero
        # projection
        Wp = np.dot(eigvec[:, idxp],
                    np.dot(np.diag(eiglam[idxp]), eigvec[:, idxp].T))
        Wm = np.dot(eigvec[:, idxm],
                    np.dot(np.diag(eiglam[idxm]), eigvec[:, idxm].T))

        # update V vector
        Rmat[ii] = Wp / mu
        Zmat[ii] = -Wm

    R = block2vec(Rmat)
    zvec = block2vec(Zmat)
    res = bvec - Amat.dot(R)
    dres = (AmatT).dot(y) - cvec + zvec

    return R, zvec, res, dres, y


def yzupdateCG(R, Rmat, Amat, AmatT, AAt, AAt_inv, cvec, zvec, Zmat, res, mu,
               epsilon_inner, y, blockstruct, bvec):
    # CG update to inner iteration
    delta_inner = 100
    tau = 1.6
    iter_max = 100
    iterations = 0

    while iterations <= iter_max:
        rhs = Amat.dot(cvec - zvec) + tau * mu * res
        y, status = cg_solve(AAt, rhs, tol=epsilon_inner, x0 = y)# M=AAt_inv) #sovles AAt y = A(Z + C) + (A(X) - b)/sigma by c.g. method
        # y, status = bicgstab(AAt, rhs, tol=epsilon_inner,
        #                      x0=y, )  # M=AAt_inv) #sovles AAt y = A(Z + C) + (A(X) - b)/sigma by c.g. method
        y = y.reshape((-1, 1))
        if status != 0:
            raise Exception("CG ERROR: NO CONVERGENCE")

        # form w(y, X, sigma)
        W = mu * R + (AmatT).dot(y) - cvec
        Wblock = vec2block(blockstruct, W)

        # compute projection onto PSD and NSD sets for each block
        for ii in range(len(blockstruct)):
            # decompose Wblock
            eiglam, eigvec = np.linalg.eigh(Wblock[ii])
            # set up matrices
            Wp = np.zeros_like(Wblock[ii]);
            Wm = np.zeros_like(Wblock[ii])
            idxp = np.where(eiglam >= 0)[0]  # get indices of eigs >= zero
            idxm = np.where(eiglam < 0)[0]  # get indices of eigs < zero
            # projection
            Wp = np.dot(eigvec[:, idxp],
                        np.dot(np.diag(eiglam[idxp]), eigvec[:, idxp].T))
            Wm = np.dot(eigvec[:, idxm],
                        np.dot(np.diag(eiglam[idxm]), eigvec[:, idxm].T))

            # update V vector
            Rmat[ii] = Wp / mu
            Zmat[ii] = -Wm

        R = block2vec(Rmat)
        zvec = block2vec(Zmat)
        res = bvec - Amat.dot(R)
        dres = (AmatT).dot(y) - cvec + zvec

        delta_inner = np.linalg.norm(res)

        if delta_inner < epsilon_inner:
            break

        iterations += 1

    return R, zvec, res, dres, y


def banner():
    str1 = "\n\n"
    str1 += "".join(['-'] * 50)
    str1 += "\n\n\tBPSDP by Nicholas C. Rubin\n"
    str1 += "\tVersion 0.0.1\n\n"
    str1 += "".join(["-"] * 50)
    str1 += "\n"

    print(str1)


def solve_bpsdp(SDP):
    """
    Solve a SDP with the boundary point method
    """
    if not isinstance(SDP, sdp_mod.SDP):
        raise TypeError(
            "SDP input {} is not an sdpsolve SDP object".format(SDP.__repr__))

    # banner()

    master_start = time.time()

    # Do read of SDP program from file
    blockstruct = SDP.blockstruct
    Amat = SDP.Amat
    bvec = SDP.bvec
    cvec = SDP.cvec
    nv = SDP.nv
    nc = SDP.nc
    Norm = SDP.Norm
    eta = SDP.eta
    epsilon = SDP.epsilon
    epsilon_inner = SDP.epsilon_inner
    iter_max = SDP.iter_max
    inner_solve = SDP.inner_solve
    admm_style = SDP.admm_style

    if inner_solve == 'CG' and epsilon_inner > epsilon / 100 and admm_style:
        raise ConvergenceWarning("For the CG inner solve should be 10x less than the outer iteration solve")

    if SDP.disp:
        print("\n\tNumber of Variables {}".format(nv))
        print("\tNumber of Constraints {}".format(nc))
        print("\tBlockstructure {}\n\n".format(",".join(map(str, blockstruct))))
        print("\tIMPORTED VARIABLES FROM input.py")
        print("\tNorm for residual = {}".format(Norm))
        print("\tEta for primal feasibility reduction = {:3.10E}".format(eta))
        print("\tepsilon for convergence = {:3.10E}".format(epsilon))
        print("\tepsilon for inner solve = {:3.10E}".format(epsilon_inner))
        print("\titer_max = {}".format(iter_max))
        print("\tinner_solve = {}".format(inner_solve))
        print("\n\n")

    #######################################################################
    #
    # Initialize factored primal and dual solution.
    # Initialize sigma, gamma
    #
    #######################################################################

    # if SDP.initial_primal is not None:
    #     R = vec2block(blockstruct, SDP.initial_primal)
    #     print(R)
    #     # y = Amat.T.dot()
    # else:
    R = []
    for i in blockstruct:
        R.append(np.eye(i).flatten(order='F'))
    R = np.require(np.hstack(R), dtype=float, requirements=['F', 'A', 'W', 'O'])
    R = R.reshape((R.shape[0], 1))
    y = np.require(np.asfortranarray(np.zeros((nc, 1))), dtype=float,
                       requirements=['F', 'A', 'W', 'O'])

    sigma = 5
    mu = 1. / sigma
    gamma = 10  # np.sqrt(10)

    zvec = np.copy(cvec)
    zvec *= -1
    vvec = np.copy(R)
    Rmat = vec2block(blockstruct, R)
    Zmat = vec2block(blockstruct, zvec)
    Vmat = vec2block(blockstruct, vvec)
    Cmat = vec2block(blockstruct, cvec)

    #######################################################################
    #
    # Calculate Cholesky decomposition of AAt
    # prepcompute norms for relative errors
    #
    #######################################################################

    AmatT = Amat.T
    AAt = Amat.dot(AmatT)
    # get inverse for conditioning CG requirements
    AAt_inv = np.zeros_like(AAt)  # inv_sp(AAt)
    if inner_solve == "EXACT":
        AAt = AAt.todense()
        LowerCho, lower_bool = sp.linalg.cho_factor(AAt)

    # temporary for test
    # Amat = Amat.todense()
    # AmatT.todense()

    normb = np.linalg.norm(bvec)
    normC = 0
    for ii in range(len(blockstruct)):
        normC += np.sqrt(np.trace(np.dot(Cmat[ii], Cmat[ii])))

    # starting residual
    res = bvec - Amat.dot(R)
    done = 0  # stopping
    update_it = 300 # update sigma every update_it iterations
    iterc = 0

    # print("Iter\tPrimal\t\tDual\t\tRes\t\tDRes\n")
    # outer iterations
    err_p_last = 1
    while not done and iterc < iter_max:

        # solve the inner problem for y
        if inner_solve == "EXACT":
            if admm_style:
                R, zvec, res, dres, y = yzupdateExact_admm_style(R, Rmat, Amat, AmatT, cvec,
                                                      zvec, Zmat, res, mu,
                                                      blockstruct, bvec, LowerCho,
                                                      lower_bool)
            else:
                R, zvec, res, dres, y = yzupdateExact(R, Rmat, Amat, AmatT, cvec,
                                                  zvec, Zmat, res, mu,
                                                  blockstruct, epsilon_inner, bvec, LowerCho,
                                                  lower_bool)

        else:
            if admm_style:
                R, zvec, res, dres, y = yzupdateCG_admm_style(R, Rmat, Amat, AmatT, AAt, cvec, zvec, Zmat, res,
                                                   mu, epsilon_inner, y,
                                                   blockstruct,
                                                   bvec)
            else:
                R, zvec, res, dres, y = yzupdateCG(R, Rmat, Amat, AmatT, AAt,
                                                   AAt_inv, cvec, zvec, Zmat, res,
                                                   mu, epsilon_inner, y,
                                                   blockstruct,
                                                   bvec)

        # calculate primal, dual energies and residual errors
        err_p = np.linalg.norm(res)
        err_d = np.linalg.norm(dres) 
        primal = np.dot(cvec.T, R)
        dual = np.dot(bvec.T, y)
        tMat = vec2block(blockstruct, R)

        iterc += 1  # k = k + 1

        err_p_last = err_p
        # check for reduction of sigmaA
        rel_err_p = err_p / (1 + normb)
        rel_err_d = err_d / (1 + normC)

        # if iterc % 500 == 0:
        if SDP.disp:
            print("%i\t%2.6e\t%2.6e\t%2.6e\t%2.6e\t%2.6e\t%2.6e" % (
            iterc, primal, dual,
            rel_err_p, rel_err_d, 1. / mu, np.trace(tMat[0])))

        # check stopping conditions
        if max(rel_err_p, rel_err_d) < epsilon:
            done = True

        ###Povh penalty update
        # if rel_err_p > rel_err_d and iterc%update_it == 0:
        #    sigma = sigma*0.9

        # Wiegle, Rendl update
        # if rel_err_d * 100 < rel_err_p:
        #     # reduce sigma
        # elif 2 * rel_err_d > rel_err_p:
        #     # increase sigma

        ##Mazziotti Penalty update.  every update_it iterations...dePrince picks update_it = 200
        if iterc % update_it == 0 or np.abs(np.log(rel_err_p/rel_err_d)) >= 3.0: # 1.60:
        # if iterc % update_it == 0 or np.abs(np.log(err_p/err_d)) >= 1.60:
        # if iterc % update_it == 0:
            # sigma = sigma*rel_err_d/rel_err_p
            # print "updating mu"
            mu = mu * rel_err_p / rel_err_d

    # print "Total Time = ", time.time() - master_start

    SDP.primal = R;
    SDP.dual = (y, zvec)


def solve_bpsdp_max(SDP):
    master_start = time.time()

    blockstruct = SDP.blockstruct
    Amat = SDP.Amat
    bvec = SDP.bvec
    cvec = SDP.cvec
    nv = SDP.nv
    nc = SDP.nc
    Norm = SDP.Norm
    eta = SDP.eta
    epsilon = SDP.epsilon
    epsilon_inner = SDP.epsilon_inner
    iter_max = SDP.iter_max
    inner_solve = SDP.inner_solve

    if SDP.disp:
        print("\n\tNumber of Variables {}".format(nv))
        print("\tNumber of Constraints {}".format(nc))
        print("\tBlockstructure {}\n\n".format(",".join(map(str, blockstruct))))
        print("\tIMPORTED VARIABLES FROM input.py")
        print("\tNorm for residual = {}".format(Norm))
        print("\tEta for primal feasibility reduction = {:3.10E}".format(eta))
        print("\tepsilon for convergence = {:3.10E}".format(epsilon))
        print("\titer_max = {}".format(iter_max))
        print("\tinner_solve = {}".format(inner_solve))
        print("\n\n")


    #######################################################################
    #
    # Initialize factored primal and dual solution.
    # Initialize sigma, gamma
    #
    #######################################################################

    R = []
    for i in blockstruct:
        R.append(np.eye(i).flatten(order='F'))
        # R.append(np.zeros((i,i)).flatten(order='F'))
    R = np.require(np.hstack(R), dtype=float, requirements=['F', 'A', 'W', 'O'])
    R = R.reshape((R.shape[0], 1))
    y = np.require(np.asfortranarray(np.zeros((nc, 1))), dtype=float,
                   requirements=['F', 'A', 'W', 'O'])
    # y += 1 #set to one for debugging purposes. set to zero when in production

    sigma = 5
    gamma = np.sqrt(10)

    zvec = np.zeros_like(cvec)
    Rmat = vec2block(blockstruct, R)
    Zmat = vec2block(blockstruct, zvec)
    Cmat = vec2block(blockstruct, cvec)

    #######################################################################
    #
    # Calculate Cholesky decomposition of AAt
    # prepcompute norms for relative errors
    #
    #######################################################################


    AAt = Amat.dot(Amat.T)
    AAt = AAt.todense()
    L, lower_bool = sp.linalg.cho_factor(AAt)
    normb = np.linalg.norm(bvec)
    normC = 0
    for ii in range(len(blockstruct)):
        normC += np.trace(np.dot(Cmat[ii], Cmat[ii]))

    # starting residual
    res = Amat.dot(R) - bvec
    done = 0  # stopping
    update_it = 2000  # update sigma every update_it iterations
    iterc = 0

    print("Iter\tPrimal\t\tDual\t\tRes\t\tDRes\n")
    # outer iterations
    while done == 0 and iterc < iter_max:

        # solve the inner problem for y
        rhs = Amat.dot(cvec + zvec) + res / sigma

        y = sp.linalg.cho_solve((L, lower_bool),
                                rhs)  # solves AAt y = A(Z+C) + (A(X) - b)/sigma

        # form w(y, X, sigma)
        W = (Amat.T).dot(y) - cvec - (R / sigma)

        Wblock = vec2block(blockstruct, W)

        # compute projection onto PSD and NSD sets for each block
        for ii in range(len(blockstruct)):

            eiglam, eigvec = np.linalg.eigh(Wblock[ii])
            idx = 0
            Wp = np.zeros_like(Wblock[ii])
            Wm = np.zeros_like(Wblock[ii])
            while idx < eiglam.shape[0]:
                if eiglam[idx] > 0:
                    Wp += eiglam[idx] * np.outer(eigvec[:, idx], eigvec[:, idx])
                    idx += 1
                else:
                    Wm += eiglam[idx] * np.outer(eigvec[:, idx], eigvec[:, idx])
                    idx += 1

            # update X vector
            if iterc > 100:
                Rmat[ii] = -sigma * Wm
            else:
                Rmat[ii] = -sigma * Wm
            Zmat[ii] = Wp

        # vectorize and calculate residuals
        R = block2vec(Rmat)
        zvec = block2vec(Zmat)
        res = Amat.dot(R) - bvec
        dres = zvec - (Amat.T).dot(y) + cvec

        err_p = np.linalg.norm(res)
        err_d = np.linalg.norm(dres)

        # calculate primal, dual energies and residual errors
        primal = np.dot(cvec.T, R)
        dual = np.dot(bvec.T, y)
        print("%i\t%2.6e\t%2.6e\t%2.6e\t%2.6e\t%2.6e" % (iterc, primal, dual,
                                                         err_p, err_d, sigma))

        iterc += 1  # k = k + 1

        # check stopping conditions
        if max(np.linalg.norm(res), np.linalg.norm(dres)) < epsilon:
            done = True

        # check for reduction of sigmaA
        rel_err_p = err_p / (1 + normb)
        rel_err_d = err_d / (1 + normC)

        ##################################
        #
        # Povh penalty update
        #
        ##################################

        # if sigma is large than inner problem gets difficult while dual
        # (primal variable)
        # feasibility is reached more easily.  if sigma is too small than we
        # reach primal feasibility easily but overall convergence may be
        # slow..dual may be slow
        if iterc % update_it == 0:
            if rel_err_d > rel_err_p:
                sigma = sigma * 10
            else:
                sigma = sigma * 0.1

    # np.save("R.npy", R)
    print("Total Time = ", time.time() - master_start)

    SDP.primal = R
    SDP.dual = (y, zvec)

