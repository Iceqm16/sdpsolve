'''
Generate input SDPA format and run csdp with subprocess.  Return results by reading solution.

'''
from sdpsolve.sdp import io_sdpsolve as io
from sdpsolve.utils.matreshape import *

def solve_csdp(SDP):

    pathToCSDP = '/Users/nick/src/Csdp-6.1.1/solver/'
    solfile = "solSDPA.sol"
    outfile = "outSDPA.out"
    infile = "inSDPA.sdpa"

    #io.writeSDPA(SDP, infile)
    #call = " ".join([pathToCSDP + "/csdp", infile, solfile, "> ", outfile])
    #p = sp.call(call , shell=True)

    #readOutput
    results = io.readSDPAout(solfile, outfile, SDP)

    if results['success'] == 0:

        SDP.primal = block2vec(results['PrimalMats'])
        SDP.dual = (results['multipliers'], block2vec(results['DualMats']))

    return results
