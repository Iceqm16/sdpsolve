import matplotlib.pyplot as plt
import numpy as np

if __name__ == "__main__":
    # set1 = np.load("truss1_alpha_0.25_beta_0.0.npy")
    # set2 = np.load("truss1_alpha_0.25_beta_0.6.npy")
    # set3 = np.load("truss1_alpha_0.25_beta_0.25.npy")
    # set4 = np.load("truss1_alpha_0.25_beta_0.125.npy")
    # set5 = np.load("truss1_alpha_0.25_beta_-0.125.npy")
    # set6 = np.load("truss1_alpha_0.25_beta_-0.25.npy")

    # plt.semilogy(range(set2.shape[0]), set2, 'C1o-', label='0.6')
    # plt.semilogy(range(set3.shape[0]), set3, 'C2o-', label='0.25')
    # plt.semilogy(range(set4.shape[0]), set4, 'C3o-', label='0.125')
    # plt.semilogy(range(set1.shape[0]), set1, 'C0o-', label='0')
    # plt.semilogy(range(set5.shape[0]), set5, 'C4o-', label='-0.125')
    # plt.semilogy(range(set6.shape[0]), set6, 'C5o-', label='-0.25')
    # plt.legend(loc='upper right')
    # plt.show()


    set1 = np.load("HubK1DN6U5DQG_alpha_0.25_beta_0.npy")
    set2 = np.load("HubK1DN6U5DQG_alpha_0.25_beta_-0.125.npy")
    set3 = np.load("HubK1DN6U5DQG_alpha_0.25_beta_-0.25.npy")
    set4 = np.load("HubK1DN6U5DQG_alpha_0.25_beta_-0.5.npy")

    plt.semilogy(range(set1.shape[0]), set1, 'C0o-', label='0')
    plt.semilogy(range(set2.shape[0]), set2, 'C1o-', label='-0.125')
    plt.semilogy(range(set3.shape[0]), set3, 'C2o-', label='-0.25')
    plt.semilogy(range(set4.shape[0]), set4, 'C3o-', label='-0.5')
    plt.legend(loc='upper right')
    plt.show()