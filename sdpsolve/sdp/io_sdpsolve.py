from scipy.sparse import coo_matrix
from sdpsolve.utils.matreshape import *


def getInOut():
    """
    Prepare program get SDP file
    """
    files = {}  # dictionary of files, sdfile, outfile
    if len(sys.argv) >= 2:
        if len(sys.argv) > 2:
            files['outfile'] = sys.argv[2]
            files['sdpfile'] = sys.argv[1]
        else:
            files['sdpfile'] = sys.argv[1]
    else:
        raise TypeError(
            "ERROR: first input should be SDP file. Second input is optional solution file")

    return files


def writeSDP(sdpfile, nc, nv, nnz, nb, A, b, cvec, blocksize):
    """
    Write an SDP to the Mazziotti format
    """
    with open(sdpfile, 'w') as fid:
        # write top line
        fid.write("%i %i %i %i\n" % (nc, nv, nnz, nb))

        # write blocksize
        for bb in blocksize:
            fid.write("%i\n" % bb)

        # # write A matrix
        # for row in range(A.shape[0]):
        #     for col in np.nonzero(A[row, :])[0]:
        #         if not np.isclose(np.abs(A[row, col]), 0):
        #             fid.write(
        #                 "%i %i %5.10E\n" % (row + 1, col + 1, A[row, col]))
        row, col = A.nonzero()
        for r, c in zip(row, col):
            fid.write("%i %i %5.10E\n" % (r + 1, c + 1, A[r, c]))

        # write b vector
        for i in range(b.shape[0]):
            fid.write("%5.10E\n" % b[i, 0])

        # write c vector
        for i in range(cvec.shape[0]):
            fid.write("%5.10E\n" % cvec[i, 0])


def readSDP(sdpfile):
    """
    Read SDP file in Mazziotti format
    """
    # use with open to handle errors
    with open(sdpfile, 'r') as fid:
        line1 = fid.readline()
        line1 = line1.split(' ')
        try:
            nc = int(line1[0])
            nv = int(line1[1])
            nnz = int(line1[2])
            nb = int(line1[3].strip())
        except ValueError:
            line1 = line1[0].split('\t')
            nc = int(line1[0])
            nv = int(line1[1])
            nnz = int(line1[2])
            nb = int(line1[3].strip())

        i = 0
        blocksize = []
        while i < nb:
            blocksize.append(int(fid.readline().split('\n')[0]))
            i += 1

        i = 0
        Amatrow = np.zeros(nnz)
        Amatcol = np.zeros(nnz)
        Amatarc = np.zeros(nnz)
        while i < nnz:
            line = list(filter(None, fid.readline().strip().split('\t')))
            if len(line) == 1:
                line = list(filter(None, line[0].split(' ')))
            # line = [x for x in line if x!='']
            Amatrow[i] = int(line[0]) - 1  # added the -1 to avoid overflow
            Amatcol[i] = int(line[1]) - 1
            Amatarc[i] = float(line[2])
            i += 1

        i = 0
        bvec = np.zeros([nc])
        while i < nc:
            line = float(fid.readline().strip())
            bvec[i] = line
            i += 1

        i = 0
        cvec = np.zeros([nv])
        while i < nv:
            cvec[i] = float(fid.readline().strip())
            i += 1

    return blocksize, Amatrow, Amatcol, Amatarc, bvec, cvec, nc, nv, nnz, nb


def sdpa_var_to_sdp_standard(block_to_offset, block_sizes, block_idx, i, j):
    return block_to_offset[block_idx] + (i - 1) * block_sizes[block_idx - 1] + (j - 1)


def read_sdpa_input(input_file):
    """
    Read the sdpa-input file and return an SDP object

    :param input_file: SDP input file in SDPA format
    :return: sdp.SDP object
    """
    with open(input_file, 'r') as fid:
        line = fid.readline().strip()
        while line[0] == '#' or line[0] == '*' or line[0] == '"':
            line = fid.readline().strip()

        num_constraint_mats = int(line.strip())

        line = fid.readline()

        num_blocks = int(line.strip().split(' ')[0])  # this might fail if comments come after the blocks

        # block size can be delimited by '{', '}', ','
        block_sizes = fid.readline()
        if '{' in block_sizes:
            block_sizes = block_sizes.replace('{', '')
        if '}' in block_sizes:
            block_sizes = block_sizes.replace('}', '')
        if ',' in block_sizes:
            block_sizes = block_sizes.split(',')

        if len(block_sizes) > 1:
            block_sizes = block_sizes.strip().split(' ')
        block_sizes = [abs(int(x)) for x in block_sizes]
        assert len(block_sizes) == num_blocks

        current_offset = 0
        block_to_offset = {}
        cvec_num = {}
        for i, x in enumerate(block_sizes):
            print(i, x)
            block_to_offset[i + 1] = current_offset
            cvec_num[i + 1] = np.arange(current_offset, current_offset + block_sizes[i]**2)
            current_offset += block_sizes[i]**2
        num_variables = current_offset
        num_constraint = num_constraint_mats

        # now read the objective function
        objective_ = fid.readline()
        if '{' in objective_:
            objective_ = objective_.replace('{', '')
        if '}' in objective_:
            objective_ = objective_.replace('}', '')
        if ',' in objective_:
            objective_ = objective_.replace(',', ' ')

        if len(objective_) == 1:
            objective_ = objective_.split(' ')

        objective_ = [float(x.strip()) for x in objective_.strip().split(' ')]
        assert len(objective_) == num_constraint_mats

        cvec = np.zeros(num_variables)
        count = 0
        Amat_row = []
        Amat_col = []
        Amat_val = []
        nnz_count = 0
        while fid:
            line = fid.readline()
            if len(line) == 0:
                break
            # print(line.strip().split(' '))
            constraint_idx, block_idx, row, col, val = list(filter(None, line.strip().split(' ')))
            # print(constraint_idx, block_idx, row, col, val)
            constraint_idx = int(constraint_idx)
            block_idx = int(block_idx)
            row = int(row)
            col = int(col)
            if np.isclose(constraint_idx, 0):
                if row == col:
                    primal_idx = sdpa_var_to_sdp_standard(block_to_offset, block_sizes, block_idx, row, col)
                    assert primal_idx == cvec_num[block_idx][(row - 1) * block_sizes[block_idx - 1] + (col - 1)]
                    cvec[primal_idx] = val
                    count += 1
                else:
                    primal_idx = sdpa_var_to_sdp_standard(block_to_offset, block_sizes, block_idx, row, col)
                    assert primal_idx == cvec_num[block_idx][(row - 1) * block_sizes[block_idx - 1] + (col - 1)]
                    cvec[primal_idx] = val
                    primal_idx = sdpa_var_to_sdp_standard(block_to_offset, block_sizes, block_idx, col, row)
                    assert primal_idx == cvec_num[block_idx][(col - 1) * block_sizes[block_idx - 1] + (row - 1)]
                    cvec[primal_idx] = val
                    count += 2
            else:
                # print(constraint_idx, block_idx, row, col, val)
                if row == col:
                    nnz_count += 1
                    Amat_row.append(constraint_idx - 1)
                    primal_idx = sdpa_var_to_sdp_standard(block_to_offset, block_sizes, block_idx, row, col)
                    Amat_col.append(primal_idx)
                    Amat_val.append(float(val))
                else:
                    nnz_count += 2
                    Amat_row.append(constraint_idx - 1)
                    primal_idx = sdpa_var_to_sdp_standard(block_to_offset, block_sizes, block_idx, row, col)
                    Amat_col.append(primal_idx)
                    Amat_val.append(float(val))
                    Amat_row.append(constraint_idx - 1)
                    primal_idx = sdpa_var_to_sdp_standard(block_to_offset, block_sizes, block_idx, col, row)
                    Amat_col.append(primal_idx)
                    Amat_val.append(float(val))

        blockstruct = block_sizes
        cvec = np.array(cvec).reshape((-1, 1))
        bvec = np.array(objective_).reshape((-1, 1))
        nc = num_constraint
        nv = num_variables
        nb = len(block_sizes)

        Amat = coo_matrix((Amat_val, (Amat_row, Amat_col)), shape=(num_constraint, num_variables)).tocsr()
        assert Amat.nnz == nnz_count
        nnz = Amat.nnz

        # we don't directly return the sdp file because of a circular import issue
        return nc, nv, nnz, nb, cvec, bvec, Amat, blockstruct


def readSDPAout(solfile, outfile, SDP):
    PrimalMats = vec2block(SDP.blockstruct, np.zeros((SDP.nv, 1)))
    DualMats = vec2block(SDP.blockstruct, np.zeros((SDP.nv, 1)))
    resutls = {}
    with open(solfile, 'r') as fid:
        # first line are the multipliers
        line = fid.readline()
        y = np.array(line.strip().split(' ')).reshape((SDP.nc, 1))

        while line:

            line = fid.readline()
            line = line.strip().split(' ')
            coords = map(int, line[:-1])
            if len(coords) == 0:
                break
            else:
                if coords[0] == 1:
                    if coords[2] - 1 != coords[3] - 1:
                        DualMats[coords[1] - 1][
                            coords[2] - 1, coords[3] - 1] = float(line[-1])
                        DualMats[coords[1] - 1][
                            coords[3] - 1, coords[2] - 1] = float(line[-1])
                    else:
                        DualMats[coords[1] - 1][
                            coords[2] - 1, coords[3] - 1] = float(line[-1])

                if coords[0] == 2:
                    if coords[2] - 1 != coords[3] - 1:
                        PrimalMats[coords[1] - 1][
                            coords[2] - 1, coords[3] - 1] = float(line[-1])
                        PrimalMats[coords[1] - 1][
                            coords[3] - 1, coords[2] - 1] = float(line[-1])
                    else:
                        PrimalMats[coords[1] - 1][
                            coords[2] - 1, coords[3] - 1] = float(line[-1])

    results = {'PrimalMats': PrimalMats, 'DualMats': DualMats, 'multipliers': y}
    if outfile != None:
        with open(outfile, 'r') as fid:

            line = fid.readline()
            line = line.strip().split(' ')
            while 'Iter:' == line[0]:
                line = fid.readline()
                line = line.strip().split(' ')

            success = 0 if 'solved' in line else -1
            pObj = float(fid.readline().strip().split(' ')[-1])
            dObj = float(fid.readline().strip().split(' ')[-1])
            rPinf = float(fid.readline().strip().split(' ')[-1])
            rDinf = float(fid.readline().strip().split(' ')[-1])
            gap = float(fid.readline().strip().split(' ')[-1])
            XZgap = float(fid.readline().strip().split(' ')[-1])

    results['success'] = success
    results['pObj'] = pObj
    results['dObj'] = dObj
    results['rPinf'] = rPinf
    results['rDinf'] = rDinf
    results['gap'] = gap
    results['XZgap'] = XZgap

    return results


def writeSDPA(SDP, filename):
    """
    write a SDP in SDPA format
    """
    nc = SDP.nc
    nv = SDP.nv
    nb = SDP.nb
    nnz = SDP.nnz
    Amat = SDP.Amat
    cvec = SDP.cvec
    bvec = SDP.bvec
    blockstruct = SDP.blockstruct

    # create variable map
    vmap = varmapper(blockstruct)

    with open(filename, 'w') as fid:

        fid.write("%i\n" % nc)
        fid.write("%i\n" % nb)
        for block in blockstruct:
            fid.write("%i " % block)
        fid.write("\n")
        for i in range(nc):
            fid.write("%4.10e " % (1.0 * bvec[i, 0]))
        fid.write("\n")

        # write cblocks
        Cmats = vec2block(blockstruct, cvec)
        for i in range(len(Cmats)):
            for x in range(Cmats[i].shape[0]):
                for y in range(x, Cmats[i].shape[1]):
                    if np.abs(Cmats[i][x, y]) > float(1.0E-14):
                        # put a negative sign on Cmats because CSDP is formulated as a maximization problem
                        fid.write("0 %i %i %i %4.10e\n" % (
                        i + 1, x + 1, y + 1, -1.0 * Cmats[i][x, y]))

        # write A blocks
        # get a row of the Amatrix nnz.  corresponds to the variable indices.
        constraintCount = 0
        while constraintCount < SDP.nc:

            crow = SDP.Amat.getrow(constraintCount)
            # operating_constraints = []
            for ii, dd in zip(crow.indices, crow.data):
                # if (vmap[ii][0], vmap[ii][2], vmap[ii][1]) not in operating_constraints:
                if vmap[ii][3] == 1:
                    fid.write("%i %i %i %i %4.10e\n" % (
                    constraintCount + 1, vmap[ii][0] + 1, vmap[ii][1] + 1,
                    vmap[ii][2] + 1, 1.0 * dd))
                    # operating_constraints.append(vmap[ii])
            constraintCount += 1


def varmapper(blockstruct):
    """
    Map variable to block, i, j
    vmap[i] = ( block, i, j )

    """

    # construct map
    varNum = 0
    vmap = {}
    for b in range(len(blockstruct)):

        block = blockstruct[b]
        xcnt = 0
        while xcnt < block:
            ycnt = 0
            while ycnt < block:
                if xcnt <= ycnt:
                    vmap[varNum] = (b, xcnt, ycnt, 1)
                else:
                    vmap[varNum] = (b, xcnt, ycnt, -1)

                varNum += 1
                ycnt += 1
            xcnt += 1

    return vmap


def getBlockStruct(sdpfile):
    with open(sdpfile, "r") as fid:

        line = fid.readline()
        line = list(filter(None, line.split(' ')))
        if len(line) == 1:
            line = list(filter(None, line[0].split(' ')))
        line = list(map(lambda x: x.strip(), line))

        num_blocks = int(line[-1])
        cnt = 0
        block_struct = []
        while cnt < num_blocks:
            block_struct.append(int(fid.readline().strip()))
            cnt += 1

    return block_struct
