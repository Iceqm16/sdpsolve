"""
Test reading the sdpa input file
"""
import sys
import os
import numpy as np
from sdpsolve.sdp.io_sdpsolve import read_sdpa_input, writeSDP
from sdpsolve.sdp.sdp import SDP

from sdpsolve.solvers.bpsdp import solve_bpsdp_max, solve_bpsdp
from sdpsolve.solvers.rrsdp.rrsdp import solve_rrsdp


def test_readsdpa():
    sdplib_directory = "/Users/lobster/opt/sdplib"
    sdplib_file = 'ss30.dat-s'
    sdpa_inputfile = os.path.join(sdplib_directory, sdplib_file)
    nc, nv, nnz, nb, cvec, bvec, Amat, blockstruct = read_sdpa_input(sdpa_inputfile)

    # assert np.allclose(cvec.reshape((blockstruct[0], blockstruct[0])), np.ones((blockstruct[0], blockstruct[0])))
    # assert np.allclose(bvec, np.hstack(([1.0], np.zeros(nc - 1))).reshape((-1, 1)))
    AAt = Amat.dot(Amat.T).toarray()
    w, v = np.linalg.eigh(AAt)
    neg_idx = np.where(w < -1.0E-15)[0]
    assert len(neg_idx) == 0

    sdp_obj = SDP()
    sdp_obj.nc = nc
    sdp_obj.nv = nv
    sdp_obj.nnz = nnz
    sdp_obj.nb = nb
    sdp_obj.blockstruct = blockstruct
    sdp_obj.cvec = -cvec
    sdp_obj.bvec = bvec
    sdp_obj.Amat = Amat

    sdp_obj.Initialize()
    sdp_obj.inner_solve = 'GG'
    sdp_obj.epsilon = 1.0E-5
    sdp_obj.epsilon_inner = sdp_obj.epsilon_inner / 100
    sdp_obj.iter_max = 10000
    sdp_obj.admm_style = False

    writeSDP(sdplib_file + '.sdp', sdp_obj.nc, sdp_obj.nv, sdp_obj.nnz, sdp_obj.nb,
             sdp_obj.Amat, sdp_obj.bvec, sdp_obj.cvec, sdp_obj.blockstruct)

    solve_bpsdp(sdp_obj)



if __name__ == "__main__":
    test_readsdpa()

