# LIBRARY IMPORTS
from sdpsolve.sdp.io_sdpsolve import getBlockStruct, readSDP
import sys
import numpy as np

# SPARSE IMPORTS
from scipy.sparse import csc_matrix


class SDP(object):
    """
    SDP object to submit to solver. The following are necessary parameters

    blockstruct = structure of SDP blocks as a list)
    nc = number of Constraints
    nv = number of Variables
    nnz = total number of nonzero constraint elements in all Amatrices
    nb = number of blocks...should be equal to len(blockstruct)
    Amat = csc_matrix sparse matrix representation of Amatrix.
        row = A_{i}, col = svec(A_{i}) nonzeros, val = bval
        Trace[A_{i}.K] = b_{i}

    bvec = np.require(bvec, dtype=float, requirements=['F','A','W','O'])
    cvec = np.require(bvec, dtype=float, requirements=['F','A','W','O'])

    """

    def __init__(self, sdpfile=None, outfile=None, infile=None):

        # Initialize SDP object
        self.sdpfile = sdpfile
        self.outfile = outfile
        self.infile = infile

        # get the data if initialized
        self.primal = None
        self.dual = None

        # set parameters
        self.blockstruct = None
        self.nc = None
        self.nv = None
        self.nnz = None
        self.nb = None

        self.Amat = None
        self.bvec = None
        self.cvec = None

        self.initial_primal = None


    def Initialize(self, ):

        # if sdpfile defined
        if self.sdpfile is not None:
            self.blockstruct = getBlockStruct(self.sdpfile)
            blocksize, Amatrow, Amatcol, Amatarc, bvec, cvec, nc, nv, nnz, nb = readSDP(
                self.sdpfile)
            self.nc = nc
            self.nv = nv
            self.nnz = nnz
            self.nb = nb

            Amat = csc_matrix((Amatarc, (Amatrow, Amatcol)), shape=(nc, nv))
            bvec = np.asfortranarray(
                bvec.reshape((bvec.shape[0], 1), order='F'))
            cvec = np.asfortranarray(
                cvec.reshape((cvec.shape[0], 1), order='F'))
            bvec = np.require(bvec, dtype=float,
                              requirements=['F', 'A', 'W', 'O'])
            cvec = np.require(cvec, dtype=float,
                              requirements=['F', 'A', 'W', 'O'])

            self.Amat = Amat
            self.bvec = bvec
            self.cvec = cvec

        # get input file parameters:
        if self.infile is not None:
            raise NotImplementedError("Not taking input right now...check back later")

        else:
            self.Norm = "L1"
            self.eta = 0.8
            self.DIIS = False
            self.DIIS_length = 4
            self.epsilon = float(1.0E-5)
            self.iter_max = 10000
            self.inner_iter_max = 100
            self.epsilon_inner = float(0.5E-5)
            self.inner_solve = "CG"
            self.NESTEROV = False
            self.disp = True
            self.admm_style = False

    def __repr__(self):
        return "\n".join(
            [str(key) + "\t" + str(val) for key, val in vars(self).items()])

