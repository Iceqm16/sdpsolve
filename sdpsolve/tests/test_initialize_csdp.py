import sys
sys.path.append("/Users/nick/")

from sdpsolve.sdp import sdp
from sdpsolve.solvers.csdp import solve_csdp


def test1():
    s = sdp.SDP()
    s.sdpfile="/Users/nick/sdpsolve/examples/example4/Hub1DN4U0DQG.sdp"
    s.Initialize()
    result = solve_csdp(s)

    return s, result

if __name__=="__main__":

    sdp_program, result = test1()
    print(result['success'])
