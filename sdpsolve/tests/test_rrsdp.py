import os

from sdpsolve.sdp import sdp
from sdpsolve.solvers.rrsdp import rrsdp

def test1():
    s = sdp.SDP()
    s.sdpfile="/Users/MacBookPro-Nick/opt/sdpsolve/examples/example4/Hub1DN4U0DQG.sdp"
    s.Initialize()
    rrsdp.solve_rrsdp(s)

    return s

if __name__=="__main__":

    sdp_program = test1()
