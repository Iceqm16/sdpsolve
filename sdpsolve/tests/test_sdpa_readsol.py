import sys
sys.path.append("/Users/nick/")

from sdpsolve.sdp import sdp
from sdpsolve.sdp import io_sdpsolve as io

def testsdparead():

    #initialize the SDP file
    s = sdp.SDP()
    s.sdpfile="/Users/nick/sdpsolve/examples/example7/HubK1DN6U5DQG.sdp"
    s.Initialize()

    #now we can pass 's' to readSDPAout
    solfile = "test_SDPA.sol"
    outfile = "test_SDPA.out"
    results = io.readSDPAout(solfile, outfile, s)


if __name__=="__main__":

    testsdparead()
