import os
import numpy as np
from sdpsolve.sdp import sdp
import sdpsolve.solvers.bpsdp as bpsdp

def test():
    s = sdp.SDP()
    s.sdpfile="./test.sdp"
    s.Initialize()
    bpsdp.solve_bpsdp(s)

    return s

if __name__=="__main__":

    sdp_program = test()
    xvec = sdp_program.primal
    A = sdp_program.Amat.todense()
    b = sdp_program.bvec
    cvec = sdp_program.cvec
    print(np.linalg.norm(A.dot(xvec) - b))
