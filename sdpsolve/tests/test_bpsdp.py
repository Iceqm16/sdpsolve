import os
import numpy as np
from sdpsolve.sdp import sdp
import sdpsolve.solvers.bpsdp as bpsdp

def test():
    s = sdp.SDP()
    # s.sdpfile="/Users/MacBookPro-Nick/opt/sdpsolve/examples/example4/Hub1DN4U0DQG.sdp"
    s.sdpfile="/Users/MacBookPro-Nick/opt/vqe_tomography/test.sdp"
    # s.sdpfile="/Users/MacBookPro-Nick/opt/vqe_tomography/test_vqe0.sdp"
    s.Initialize()
    bpsdp.solve_bpsdp(s)

    return s

if __name__=="__main__":

    sdp_program = test()
    xvec = sdp_program.primal
    A = sdp_program.Amat.todense()
    b = sdp_program.bvec
    cvec = sdp_program.cvec
    print(np.linalg.norm(A.dot(xvec) - b))
