import numpy as np
from sdpsolve.solvers.alternating_projections import opdm_projection_boreland_dennis


def test_opdm_projection_boreland_dennis_spin_orbital():
    true_opdm = np.array([[ 0.92064791, 0.        , 0.09921513, 0.        , -0.02130591, 0.        ],
                          [ 0.        , 0.08715753, 0.        , 0.119398  ,  0.        , 0.03807369],
                          [ 0.09921513, 0.        , 0.35889448, 0.        ,  0.16594523, 0.        ],
                          [ 0.        , 0.119398  , 0.        , 0.63164696,  0.        , 0.16442565],
                          [-0.02130591, 0.        , 0.16594523, 0.        ,  0.72045761, 0.        ],
                          [ 0.        , 0.03807369, 0.        , 0.16442565,  0.        , 0.28119551]])
    test_true_opdm = opdm_projection_boreland_dennis([true_opdm], epsilon=1.0E-8)[0]
    assert np.allclose(test_true_opdm, true_opdm, atol=1.0E-8)


def test_opdm_projection_boreland_dennis_spatial_orbital():
    true_opdm = np.array([[ 0.92064791, 0.        , 0.09921513, 0.        , -0.02130591, 0.        ],
                          [ 0.        , 0.08715753, 0.        , 0.119398  ,  0.        , 0.03807369],
                          [ 0.09921513, 0.        , 0.35889448, 0.        ,  0.16594523, 0.        ],
                          [ 0.        , 0.119398  , 0.        , 0.63164696,  0.        , 0.16442565],
                          [-0.02130591, 0.        , 0.16594523, 0.        ,  0.72045761, 0.        ],
                          [ 0.        , 0.03807369, 0.        , 0.16442565,  0.        , 0.28119551]])
    test_true_opdms = opdm_projection_boreland_dennis([true_opdm[::2, ::2], true_opdm[1::2, 1::2]], epsilon=1.0E-8)
    assert np.allclose(test_true_opdms[0], true_opdm[::2, ::2], atol=1.0E-8)
    assert np.allclose(test_true_opdms[1], true_opdm[1::2, 1::2], atol=1.0E-8)


if __name__ == "__main__":
    test_opdm_projection_boreland_dennis_spin_orbital()
    test_opdm_projection_boreland_dennis_spatial_orbital()
