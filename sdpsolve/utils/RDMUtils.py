'''
Various Utilities for debugging quantum chemistry SDP problems
'''
import numpy as np
from itertools import product


def sdpout(outfile):
    R = []
    y = []
    with open(outfile, 'r') as fid:

        line = fid.readline()
        while ("Factored_Primal_Solution=" not in line):
            line = fid.readline()
        while ("Dual_Solution=" not in line):
            line = fid.readline()
            try:
                R.append(float(line))
            except:
                break
        while ("Total Time" not in line):
            line = fid.readline()
            try:
                y.append(float(line))
            except:
                break
    return np.array(R).reshape((len(R), 1), order='F'), np.array(y).reshape(
        (len(y), 1), order='F')


def readEI(eifile, nao, nocc):
    H1 = np.zeros((nao, nao))
    V2 = np.zeros((nao * nao, nao * nao))
    with open(eifile, 'r') as fid:
        line = fid.readline()

        while (line):

            line = fid.readline()
            try:
                line = map(float, line.split('\t'))
            except ValueError:
                break

            if line[2] == 0.0:
                H1[int(line[0]) - 1, int(line[1]) - 1] = line[4]
                H1[int(line[1]) - 1, int(line[0]) - 1] = line[4]
            else:
                V2[(int(line[0]) - 1) * nao + int(line[1]) - 1,
                   (int(line[2]) - 1) * nao + int(line[3]) - 1] = line[4]

    K2 = np.zeros((nao * nao, nao * nao))
    for i in range(nao):
        for j in range(nao):
            for k in range(nao):
                for l in range(nao):

                    if i == k:
                        K2[i * nao + j, k * nao + l] += (1. / (2 * nocc - 1)) * \
                                                        H1[j, l]
                    if j == l:
                        K2[i * nao + j, k * nao + l] += (1. / (2 * nocc - 1)) * \
                                                        H1[i, k]

                    K2[i * nao + j, k * nao + l] += V2[i * nao + j, k * nao + l]

    return K2


def wedge(D1):
    # only works on canonical because loops rely on that ordering
    d2wedge = np.zeros((D1.shape[0] ** 2, D1.shape[1] ** 2))
    L = D1.shape[0]
    for i in range(L):
        for j in range(L):
            for k in range(L):
                for l in range(L):
                    d2wedge[i * L + j, k * L + l] = D1[i, k] * D1[j, l] - D1[
                                                                              j, k] * \
                                                                          D1[
                                                                              i, l]

    return d2wedge


def SpinAdapt(nao):
    U = np.zeros((nao ** 2, nao ** 2))
    d2_bas = dict(zip(range(nao * nao), list(product(range(nao), range(nao)))))
    d2_bas_rev = dict(zip(d2_bas.values(), d2_bas.keys()))

    d2a_bas = {}
    cnt = 0
    for i in range(nao):
        for j in range(i + 1, nao):
            d2a_bas[cnt] = (i, j)
            cnt += 1

    d2s_bas = {}
    cnt = 0
    for i in range(nao):
        for j in range(i, nao):
            d2s_bas[cnt] = (i, j)
            cnt += 1

    cnt = 0
    for xx in sorted(d2a_bas.keys()):
        i, j = d2a_bas[xx]
        x1 = d2_bas_rev[(i, j)]
        x2 = d2_bas_rev[(j, i)]
        U[x1, cnt] = 1. / np.sqrt(2)
        U[x2, cnt] = -1. / np.sqrt(2)
        cnt += 1

    for xx in sorted(d2s_bas.keys()):
        i, j = d2s_bas[xx]
        x1 = d2_bas_rev[(i, j)]
        x2 = d2_bas_rev[(j, i)]

        if x1 == x2:
            U[x1, cnt] = 1.0
        else:
            U[x1, cnt] = 1. / np.sqrt(2)
            U[x2, cnt] = 1. / np.sqrt(2)

        cnt += 1

    return U
