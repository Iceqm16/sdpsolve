import numpy as np
import sys

##############################################################################
#
#  Matrix reshaping routines
#
##############################################################################
def vec2block(blockstruct, vector, order='F'):

    mats = []
    for i in blockstruct:
        if len(vector.shape) == 2:
            mats.append(vector[:i**2, 0].reshape((i, i),order=order))
            vector = vector[i**2:,:]
        elif len(vector.shape) == 1:
            mats.append(vector[:i ** 2].reshape((i, i), order=order))
            vector = vector[i ** 2:]
        else:
            sys.error("Can't handle reshaping rank 3 tensors")

    return mats


def vec2blockRR(blockstruct, vector):
    mats = []
    for i in blockstruct:
        if len(vector.shape) == 2:
            tmp_mat = np.asfortranarray(
                vector[:i ** 2, 0].reshape((i, i), order='F'))
            mats.append(np.dot(tmp_mat, tmp_mat.T))
            vector = vector[i ** 2:, 0]
        elif len(vector.shape) == 1:
            tmp_mat = np.asfortranarray(
                vector[:i ** 2].reshape((i, i), order='F'))
            mats.append(np.dot(tmp_mat, tmp_mat.T))
            vector = vector[i ** 2:]
        else:
            sys.error("Can't handle reshaping rank 3 tensors")

    return mats


def block2vec(mats):
    mats = map(lambda x: x.flatten(order='F'), mats)
    mats = np.require(np.hstack(mats), dtype=float,
                      requirements=['F', 'A', 'W', 'O'])

    return mats.reshape((mats.shape[0], 1))
